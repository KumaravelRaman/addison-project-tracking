﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;


namespace AddisonProjectTracking
{
    public partial class Login : System.Web.UI.Page
    {
        string cs = ConfigurationManager.ConnectionStrings["conn"].ConnectionString;
        DataControl cls = new DataControl();
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnLogin_Click(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection(cs);

            try
            {
                SqlCommand cmd = new SqlCommand("sp_b_adminlogin", con);

                cmd.CommandType = CommandType.StoredProcedure;
                con.Open();
                cmd.Parameters.AddWithValue("@qtype", "adminlogin");
                cmd.Parameters.AddWithValue("@username", txtusername.Text);
                cmd.Parameters.AddWithValue("@password", txtPassword.Text);
                SqlDataReader sdr = cmd.ExecuteReader();

                if (sdr.Read())
                {
                    Session["UserName"] = sdr["UserName"].ToString();
                    Session["EmailId"] = sdr["Email"].ToString();
                    Response.Redirect("Admin/Dashboard.aspx");

                }
                else if(txtusername.Text!="" && txtPassword.Text !="")
                    {
                    string queryy = @"select * from tbl_employee where Username='" + txtusername.Text + "' and Password='" + txtPassword.Text + "'";
                    DataTable dtt = cls.Getdata(queryy);
                    if (dtt.Rows.Count > 0)
                    {
                        Session["UserName"] = dtt.Rows[0]["EmployeeName"].ToString();
                        Session["EmailId"] = dtt.Rows[0]["Email"].ToString();
                        Session["Type"] = dtt.Rows[0]["TypeId"].ToString();
                        if (dtt.Rows[0]["TypeId"].ToString() == "1")
                            Response.Redirect("Users/Dashboard.aspx");
                        else if (dtt.Rows[0]["TypeId"].ToString() == "2")
                            Response.Redirect("Users/SearchProject.aspx");
                        else if (dtt.Rows[0]["TypeId"].ToString() == "3")
                            Response.Redirect("Users/SearchProject.aspx");

                    }
                }
                else
                {
                    Label1.Text = "Sorry Invalid User Name or Password";
                }
            }
            catch (Exception ex)
            {
                Response.Write(ex);
            }
            finally
            {
                con.Close();

            }
        }
    }
}