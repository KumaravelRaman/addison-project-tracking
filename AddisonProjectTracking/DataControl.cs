﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using System.Web;
using System.IO;
using System.Web.UI;
using System.Globalization;
using System.Security.Cryptography;
using System.Net.Mail;
using System.Net;

/// <summary>
/// Summary description for CllClass
/// </summary>
public class DataControl
{
    public DataControl()
    {
        DynamicParameters = new Dictionary<string, object>();
        //
        // TODO: Add constructor logic here
        //
    }
   
    
    public string con = ConfigurationManager.ConnectionStrings["conn"].ConnectionString;
    public SqlCommand comm = new SqlCommand();
    public Dictionary<string, object> DynamicParameters { get; set; }
    public DataTable Getdata(string sqlstr)
    {
        SqlDataAdapter da = new SqlDataAdapter(sqlstr, con);
        da.SelectCommand.CommandTimeout = 50000;
        DataTable dt = new DataTable();
        da.Fill(dt);
        return dt;
    }
    public string Encrypt(string clearText)
    {
        string EncryptionKey = "MAKV2SPBNI99212";
        byte[] clearBytes = Encoding.Unicode.GetBytes(clearText);
        using (Aes encryptor = Aes.Create())
        {
            Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
            encryptor.Key = pdb.GetBytes(32);
            encryptor.IV = pdb.GetBytes(16);
            using (MemoryStream ms = new MemoryStream())
            {
                using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateEncryptor(), CryptoStreamMode.Write))
                {
                    cs.Write(clearBytes, 0, clearBytes.Length);
                    cs.Close();
                }
                clearText = Convert.ToBase64String(ms.ToArray());
            }
        }
        return clearText;
    }
    public string Decrypt(string cipherText)
    {
        string EncryptionKey = "MAKV2SPBNI99212";
        cipherText = cipherText.Replace(" ", "+");
        byte[] cipherBytes = Convert.FromBase64String(cipherText);
        using (Aes encryptor = Aes.Create())
        {
            Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
            encryptor.Key = pdb.GetBytes(32);
            encryptor.IV = pdb.GetBytes(16);
            using (MemoryStream ms = new MemoryStream())
            {
                using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateDecryptor(), CryptoStreamMode.Write))
                {
                    cs.Write(cipherBytes, 0, cipherBytes.Length);
                    cs.Close();
                }
                cipherText = Encoding.Unicode.GetString(ms.ToArray());
            }
        }
        return cipherText;
    }
    public string GetSingleData(string sqlstr)
    {
        SqlDataAdapter da = new SqlDataAdapter(sqlstr, con);
        DataTable dt = new DataTable();
        da.Fill(dt);
        if (dt.Rows.Count > 0)
            return dt.Rows[0][0].ToString();
        else
            return "0";
    }

    public void excelReport(DataTable dt, string filename)
    {
        string admin = "Admin";
        try
        {
            string filePath = HttpContext.Current.Server.MapPath(".") + "\\Reports\\";
            if (!Directory.Exists(filePath))
                Directory.CreateDirectory(filePath + "\\Reports\\");
            string excelpath = filePath + "\\" + ("Reports" + ".xls");
            if (File.Exists(excelpath))
                File.Delete(excelpath);
            FileInfo file = new FileInfo(excelpath);
            DataTable dtCloned = dt.Clone();
            for (int i = 0; i < dtCloned.Columns.Count; i++)
            {
                string col = dtCloned.Columns[i].ColumnName.ToLower();
                if (col == "status")
                    dtCloned.Columns[i].DataType = typeof(string);
                if (col == "rownumber" || col == "slno" || col == "id" || col == "customer_id")
                {
                    dtCloned.Columns.RemoveAt(i);
                    i = i - 1;
                }
            }
            foreach (DataRow row in dt.Rows)
            {
                dtCloned.ImportRow(row);
            }
            foreach (DataRow row in dtCloned.Rows)
            {
                if (dtCloned.Columns.Contains("status") == true)
                {
                    string status = "";
                    if (row.Field<string>("status") == "1" || row.Field<string>("status") == "True" || row.Field<string>("status") == "Active")
                        status = "Active";
                    else if (row.Field<string>("status") == "0" || row.Field<string>("status") == "False" || row.Field<string>("status") == "InActive") status = "InActive";
                    else status = row.Field<string>("status");
                    row.SetField("status", status);
                }
            }
            int colcount = dtCloned.Columns.Count;
            //string created_by = HttpContext.Current.Session["UserName"].ToString();
            StreamWriter writter = new StreamWriter(excelpath, true);
            writter.WriteLine("<html>");
            writter.WriteLine("<head>");
            writter.WriteLine("<body>");
            writter.WriteLine("<table border='1'><tr><td colspan='" + colcount + "' style='font-family:Calibri;text-align:center;padding: 10px;background-color: #08bff5;font-size:14px'><b>REPORTS</b></td></tr>");
            writter.WriteLine("<tr><td colspan='" + ((colcount / 2) + (colcount % 2)) + "' style='color: blue;'><b>Report Date : " + DateTime.Now + "</b> </td><td colspan='" + (colcount / 2) + "' style='color: blue;'><b>Created By : "+admin+"</b></td><tr>");
            for (int i = 0; i < dtCloned.Columns.Count; i++)
            {
                writter.WriteLine("<td> <b>" + dtCloned.Columns[i].ToString().ToUpper() + "</b></td>");
            }
            writter.WriteLine("</tr>");
            for (int i1 = 0; i1 < dtCloned.Rows.Count; i1++)
            {
                writter.WriteLine("<tr>");
                for (int i2 = 0; i2 < dtCloned.Columns.Count; i2++)
                    writter.WriteLine("<td >" + dtCloned.Rows[i1][i2].ToString().ToUpper() + "</td>");
                writter.WriteLine("</tr>");
            }
            writter.WriteLine("</table>");
            writter.WriteLine("</body>");
            writter.WriteLine("</head>");
            writter.WriteLine("</html>");
            writter.Close();
            HttpContext.Current.Response.ContentType = "application/ms-excel";
            HttpContext.Current.Response.AddHeader("content-disposition", "inline; filename=" + filename + "");
            HttpContext.Current.Response.TransmitFile(excelpath);
            HttpContext.Current.Response.Flush();
            HttpContext.Current.Response.End();
        }
        catch (Exception e)
        { }
    }

    public bool InsertOrUpdateData(string argstrQuery, Boolean IsSp = false, Boolean IsParameter = false)
    {
        Boolean IsSaved = false;
        using (SqlConnection connection = new SqlConnection(con))
        {
            try
            {
                connection.Open();
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = connection;
                cmd.CommandText = argstrQuery;
                if (IsSp)
                    cmd.CommandType = CommandType.StoredProcedure;
                else
                    cmd.CommandType = CommandType.Text;
                if (IsParameter)
                {
                    if (DynamicParameters.Count > 0)
                    {
                        foreach (KeyValuePair<string, object> item in DynamicParameters)
                            cmd.Parameters.AddWithValue(item.Key, item.Value);
                        int i = cmd.ExecuteNonQuery();
                        if (i != 0)
                            IsSaved = true;
                    }
                }
                else
                {
                    int i = cmd.ExecuteNonQuery();
                    if (i != 0)
                        IsSaved = true;
                }
            }
            catch (Exception ex)
            {
                if (connection != null) ((IDisposable)connection).Dispose();
            }
        }
        return IsSaved;
    }
    public void sendmailnew(string toname, string subject, string body)
    {
            toname = "swathy@brainmagic.info";
            subject = "Test";
            body = "HI";
            MailMessage mm = new MailMessage(toname, toname)
            {
                Subject = subject,
                IsBodyHtml = true,
                Body = body
            };
       
            string host = "webmail.brainmagic.info";
            string from = "swathy@brainmagic.info";
            string password = "swathy@123";
            int port = 587;
            using (var client = new System.Net.Mail.SmtpClient(host, port))
            {
                client.Credentials = new NetworkCredential(from, password);
                client.EnableSsl = true;
                try
                {
                    Console.WriteLine("Attempting to send email...");
                    client.Send(mm);
                    Console.WriteLine("Email sent!");
                }
                catch (Exception ex)
                {
                    Console.WriteLine("The email was not sent.");
                    Console.WriteLine("Error message: " + ex.Message);
                }
            }
        //SmtpClient smtp = new SmtpClient
        //    {
        //        Host =host ,
        //        Port = Convert.ToInt32(587),
        //        EnableSsl = true,
        //        DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network,
        //        UseDefaultCredentials = false,
        //        //Timeout=5000,
        //        Credentials = new System.Net.NetworkCredential(from, password)
        //    };
            //smtp.Send(mm);
        
    }

    public DataTable GetDetails(string argstrQuery, Boolean IsSp = false, Boolean IsParameter = false, Boolean isCombo = false, string argstrIntialValue = "--Select--")
    {
        DataTable dtData = null;
        using (SqlConnection connection = new SqlConnection(con))
        {
            try
            {
                connection.Open();
                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = argstrQuery;
                cmd.Connection = connection;
                if (IsSp)
                {
                    if (IsParameter)
                    {
                        if (DynamicParameters.Count > 0)
                        {
                            foreach (KeyValuePair<string, object> item in DynamicParameters)
                                cmd.Parameters.AddWithValue(item.Key, item.Value);
                        }
                    }
                    cmd.CommandType = CommandType.StoredProcedure;
                }
                else
                {
                    if (IsParameter)
                    {
                        if (DynamicParameters.Count > 0)
                        {
                            foreach (KeyValuePair<string, object> item in DynamicParameters)
                                cmd.Parameters.AddWithValue(item.Key, item.Value);
                        }
                    }
                    cmd.CommandType = CommandType.Text;
                }

                SqlDataReader dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    dtData = new DataTable();
                    dtData.Load(dr);
                    if (isCombo)
                    {
                        DataRow drs = dtData.NewRow();
                        drs[1] = "0";
                        drs[0] = argstrIntialValue;
                        dtData.Rows.InsertAt(drs, 0);
                    }
                }
            }
            catch (SqlException ex)
            {
                if (connection != null)
                    ((IDisposable)connection).Dispose();
            }
        }
        return dtData;
    }


    public string GetFormatDate1(string date)
    {
        string[] formats = { "dd/MM/yyyy HH:mm:ss", "dd/MM/yyyy", "dd/MM/yyyy HH:mm", "dd/M/yyyy" };

        DateTime value = DateTime.ParseExact(date, formats, CultureInfo.InvariantCulture, DateTimeStyles.None);
        return value.ToString("yyyy-MM-dd", CultureInfo.InvariantCulture);
    }
}
