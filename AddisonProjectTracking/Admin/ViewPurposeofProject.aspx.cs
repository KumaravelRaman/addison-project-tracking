﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AddisonProjectTracking.Admin
{
    public partial class ViewPurposeofProject : System.Web.UI.Page
    {
        string cs = ConfigurationManager.ConnectionStrings["conn"].ConnectionString;
        public common db = new common();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                string depart = MyCrypto.GetDecryptedQueryString(Request.QueryString["Id"].ToString());

                SqlConnection con = new SqlConnection(cs);
                con.Open();
                SqlCommand cmd = new SqlCommand("Select PurposeofProject,Status,CreatedBy,UpdatedBy from tbl_purposeofproject where Id='" + depart + "'", con);
                SqlDataReader sdr = cmd.ExecuteReader();
                if (sdr.Read())
                {
                    Label2.Text = sdr["PurposeofProject"].ToString();

                    Label4.Text = sdr["CreatedBy"].ToString();
                    Label5.Text = sdr["UpdatedBy"].ToString();
                    Label3.Text = sdr["Status"].ToString();
                }
                con.Close();

            }
        }
    }
}