﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.IO;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AddisonProjectTracking.Admin
{
    public partial class ViewSubGroup : System.Web.UI.Page
    {
        string cs = ConfigurationManager.ConnectionStrings["conn"].ConnectionString;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                string subgroup = MyCrypto.GetDecryptedQueryString(Request.QueryString["Id"].ToString());

                SqlConnection con = new SqlConnection(cs);
                con.Open();
                SqlCommand cmd = new SqlCommand("Select DivisionId,SubDivision,Status,CreatedBy,UpdatedBy from tbl_subdivision where Id='" + subgroup + "'", con);
                SqlDataReader sdr = cmd.ExecuteReader();
                if (sdr.Read())
                {
                    Label2.Text = sdr["DivisionId"].ToString();
                    Label1.Text = sdr["SubDivision"].ToString();
                    Label4.Text = sdr["CreatedBy"].ToString();
                    Label5.Text = sdr["UpdatedBy"].ToString();
                    Label3.Text = sdr["Status"].ToString();
                }
                con.Close();

            }
        }
    }
}