﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Layout.Master" AutoEventWireup="true" CodeBehind="EditStatus.aspx.cs" Inherits="AddisonProjectTracking.Admin.EditStatus" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <div class="main-content">

        <div class="page-content">
            <div class="container-fluid">

                <!-- start page title -->
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">Edit Status</h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Project Status Master</a></li>
                                    <li class="breadcrumb-item active">Edit Status</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>
                <!-- end page title -->

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">

                                <h4 class="card-title">Edit Status</h4>
                                <p class="card-subtitle mb-4"></p>

                                <div class="row">
                                    <div class="col-md-6">

                                        <%-- <div class="form-group">
                                                    <label>Department Code</label>
                   <asp:TextBox ID="txtCode" CssClass="form-control" runat="server"></asp:TextBox>  
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtCode"  Display="Dynamic" ErrorMessage="Enter Code" ForeColor="Red"></asp:RequiredFieldValidator> 
                                                    
                                                </div>--%>

                                        <div class="form-group">
                                            <label>Project Status </label>
                                            <asp:TextBox ID="txtStatus" CssClass="form-control" placeholder="Enter Status" runat="server"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtStatus" Display="Dynamic" ErrorMessage="Enter Group Name" ForeColor="Red"></asp:RequiredFieldValidator>

                                        </div>
                                    </div>

                                    <div class="col-md-6">

                                        <div class="form-group">
                                            <label>Status</label>
                                            <asp:DropDownList ID="ddlStatus" CssClass="form-control" runat="server">
                                                <asp:ListItem Value="Active">Active</asp:ListItem>
                                                <asp:ListItem Value="Deactive">Deactive</asp:ListItem>
                                            </asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ControlToValidate="ddlStatus" Display="Dynamic" ErrorMessage="Choose Status" ForeColor="Red"></asp:RequiredFieldValidator>
                                        </div>
                                        
                                    </div>
                                    <div class="col-md-12 text-center">


                                            <div class="form-group">


                                                <asp:Button ID="Button1" runat="server" class="btn btn-primary sm" Text="Update" OnClick="Button1_Click" />
                                                <a href="StatusList.aspx" class="btn btn-dark btn-sm" style="line-height: 1.999">Back</a>
                                            </div>

                                        </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
