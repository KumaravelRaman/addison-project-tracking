﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AddisonProjectTracking.Admin
{
    public partial class AddDepartment : System.Web.UI.Page
    {
        string cs = ConfigurationManager.ConnectionStrings["conn"].ConnectionString;

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
             SqlConnection con = new SqlConnection(cs);

             try
             {
                 SqlCommand cmd = new SqlCommand("sp_b_department", con);

                 cmd.CommandType = CommandType.StoredProcedure;
                 con.Open();
                 cmd.Parameters.AddWithValue("@qtype", "adddepartment");
                 cmd.Parameters.AddWithValue("@department", txtDepartment.Text);
                 cmd.Parameters.AddWithValue("@Status", ddlStatus.SelectedItem.Text);
                 cmd.Parameters.AddWithValue("@CreatedOn", DateTime.Now.ToString());
                 cmd.Parameters.AddWithValue("@CreatedBy", Session["UserName"].ToString());

                 cmd.ExecuteNonQuery();
                 

             }
             catch (Exception ex)
             {
                 Response.Write(ex);
             }
             finally
             {
                 con.Close();
                 Response.Write("<script language='javascript'>alert('Department Added Successfully');window.location=('DepartmentList.aspx')</script>");
             }
        }
    }
}