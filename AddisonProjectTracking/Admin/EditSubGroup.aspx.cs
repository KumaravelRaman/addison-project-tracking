﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.IO;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AddisonProjectTracking.Admin
{
    public partial class EditSubGroup : System.Web.UI.Page
    {
        string cs = ConfigurationManager.ConnectionStrings["conn"].ConnectionString;

        protected void Page_Load(object sender, EventArgs e)
        {
           
            if (!IsPostBack)
            {
                string subgroup = MyCrypto.GetDecryptedQueryString(Request.QueryString["Id"].ToString());

                SqlConnection con = new SqlConnection(cs);
                con.Open();
                SqlCommand cmd = new SqlCommand("Select DivisionId,SubDivision,Status,CreatedBy,UpdatedBy from tbl_subdivision where Id='" + subgroup + "'", con);
                SqlDataReader sdr = cmd.ExecuteReader();
                if (sdr.Read())
                {
                    DropDownList1.SelectedValue = sdr["DivisionId"].ToString();

                    txtSubgroup.Text = sdr["SubDivision"].ToString();
                    ddlStatus.SelectedValue = sdr["Status"].ToString();
                }
                con.Close();
            }
            if (!IsPostBack)
            {
                GetData();
            }
        }
        protected void GetData()
        {
            SqlConnection conn = new SqlConnection(cs);
            conn.Open();
            string com = "Select distinct Division,Id from tbl_division where Division!=''";
            SqlDataAdapter adpt = new SqlDataAdapter(com, conn);
            DataTable dt = new DataTable();
            adpt.Fill(dt);
            DropDownList1.DataSource = dt;
            DropDownList1.DataTextField = "Division";
            DropDownList1.DataValueField = "Id";
            DropDownList1.DataBind();
            DropDownList1.Items.Insert(0, new ListItem("-- Select Division --", ""));
        }
        protected void Button1_Click(object sender, EventArgs e)
        {
            string subgroup = MyCrypto.GetDecryptedQueryString(Request.QueryString["Id"].ToString());

            SqlConnection con = new SqlConnection(cs);

            try
            {
                SqlCommand cmd = new SqlCommand("sp_b_subdivision", con);

                cmd.CommandType = CommandType.StoredProcedure;
                con.Open();
                cmd.Parameters.AddWithValue("@Id", subgroup);
                cmd.Parameters.AddWithValue("@qtype", "updatesubdivision");
                cmd.Parameters.AddWithValue("@DivisionId", DropDownList1.SelectedValue);
                cmd.Parameters.AddWithValue("@SubDivision", txtSubgroup.Text);
                cmd.Parameters.AddWithValue("@Status", ddlStatus.SelectedItem.Text);
                cmd.Parameters.AddWithValue("@UpdatedOn", DateTime.Now);
                cmd.Parameters.AddWithValue("@UpdatedBy", Session["UserName"].ToString());

                cmd.ExecuteNonQuery();


            }
            catch (Exception ex)
            {
                Response.Write(ex);
            }
            finally
            {
                con.Close();
                Response.Write("<script language='javascript'>alert('Sub Group Updated Successfully');window.location=('SubGroupList.aspx')</script>");
            }
        }
    }
}