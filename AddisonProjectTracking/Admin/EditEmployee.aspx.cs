﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AddisonProjectTracking
{
    public partial class EditEmployee : System.Web.UI.Page
    {
        string cs = ConfigurationManager.ConnectionStrings["conn"].ConnectionString;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                string subgroup = MyCrypto.GetDecryptedQueryString(Request.QueryString["Id"].ToString());

                SqlConnection con = new SqlConnection(cs);
                con.Open();
                SqlCommand cmd = new SqlCommand("Select EmployeeName,EmployeeCode,DepartmentId,TypeId,ReportingPerson,Email,Mobile,Status,CreatedBy from tbl_employee where Id='" + subgroup + "'", con);
                SqlDataReader sdr = cmd.ExecuteReader();
                if (sdr.Read())
                {
                    txtCode.Text = sdr["EmployeeCode"].ToString();
                    txtEmail.Text = sdr["Email"].ToString();
                    txtMobile.Text = sdr["Mobile"].ToString();
                    ddldepartment.SelectedValue = sdr["DepartmentId"].ToString();
                    //txtEmpname.Text = sdr["EmployeeName"].ToString();
                    txtPerson.Text = sdr["ReportingPerson"].ToString();
                    ddlType.SelectedValue = sdr["TypeId"].ToString();

                    
                    ddlStatus.SelectedValue = sdr["Status"].ToString();
                }
                con.Close();
            }
            if (!IsPostBack)
            {
                GetData();
            }
        }
        protected void GetData()
        {
            SqlConnection conn = new SqlConnection(cs);
            conn.Open();
            string com = "Select distinct Department,Id from tbl_department where Department!=''";
            SqlDataAdapter adpt = new SqlDataAdapter(com, conn);
            DataTable dt = new DataTable();
            adpt.Fill(dt);
            ddldepartment.DataSource = dt;
            ddldepartment.DataTextField = "Department";
            ddldepartment.DataValueField = "Id";
            ddldepartment.DataBind();
            ddldepartment.Items.Insert(0, new ListItem("-- Select Department --", ""));
        }
        protected void Button1_Click(object sender, EventArgs e)
        {
            string subgroup = MyCrypto.GetDecryptedQueryString(Request.QueryString["Id"].ToString());

            SqlConnection con = new SqlConnection(cs);

            try
            {
                SqlCommand cmd = new SqlCommand("sp_b_employee", con);

                cmd.CommandType = CommandType.StoredProcedure;
                con.Open();
                cmd.Parameters.AddWithValue("@qtype", "Updateemployee");
                cmd.Parameters.AddWithValue("@Id", subgroup);
                cmd.Parameters.AddWithValue("@EmployeeCode", txtCode.Text);
                cmd.Parameters.AddWithValue("@Email", txtEmail.Text);
                //cmd.Parameters.AddWithValue("@EmployeeName", txtEmpname.Text);
                cmd.Parameters.AddWithValue("@ReportingPerson", txtPerson.Text);
                cmd.Parameters.AddWithValue("@Mobile", txtMobile.Text);
                cmd.Parameters.AddWithValue("@DepartmentId", ddldepartment.SelectedValue);
                cmd.Parameters.AddWithValue("@TypeId", ddlType.SelectedValue);
                cmd.Parameters.AddWithValue("@Status", ddlStatus.SelectedItem.Text);
                cmd.Parameters.AddWithValue("@UpdatedOn", DateTime.Now);
                cmd.Parameters.AddWithValue("@UpdatedBy", Session["UserName"].ToString());
                //cmd.Parameters.AddWithValue("@UserName", txtEmpname.Text);
                cmd.Parameters.AddWithValue("@Password", txtMobile.Text);
                cmd.ExecuteNonQuery();


            }
            catch (Exception ex)
            {
                Response.Write(ex);
            }
            finally
            {
                con.Close();
                Response.Write("<script language='javascript'>alert('Employee List Updated Successfully');window.location=('EmployeeList.aspx')</script>");
            }
        }
    }
}