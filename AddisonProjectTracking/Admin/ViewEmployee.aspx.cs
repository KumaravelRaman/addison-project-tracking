﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AddisonProjectTracking
{
    public partial class ViewEmployee : System.Web.UI.Page
    {
        string cs = ConfigurationManager.ConnectionStrings["conn"].ConnectionString;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                string subgroup = MyCrypto.GetDecryptedQueryString(Request.QueryString["Id"].ToString());

                SqlConnection con = new SqlConnection(cs);
                con.Open();
                SqlCommand cmd = new SqlCommand("Select EmployeeName,EmployeeCode,DepartmentId,TypeId,ReportingPerson,Email,Mobile,Status,CreatedBy,UpdatedBy from tbl_employee where Id='" + subgroup + "'", con);
                //SqlCommand cmd1=new SqlCommand("select *, (Select Department from tbl_department b where b.id=a.DepartmentId)Department from tbl_employee a where Id='"+ subgroup +"'",con);
                //SqlCommand cmd2=new SqlCommand("select *, (Select Department from tbl_department b where b.id=a.DepartmentId)Department from tbl_employee a where Id='"+ subgroup +"'",con);
                
                SqlDataReader sdr = cmd.ExecuteReader();
                if (sdr.Read())
                {
                  lblcode.Text = sdr["EmployeeCode"].ToString();
                  lblemail.Text = sdr["Email"].ToString();
                    lblmobile.Text = sdr["Mobile"].ToString();
                  lbldepart.Text = sdr["DepartmentId"].ToString();
                  lblname.Text = sdr["EmployeeName"].ToString();
                  lblperson.Text = sdr["ReportingPerson"].ToString();
                    lbltype.Text = sdr["TypeId"].ToString();
                    lblstatus.Text = sdr["Status"].ToString();
                    lblcreatedby.Text = sdr["CreatedBy"].ToString();
                    lblupdated.Text = sdr["UpdatedBy"].ToString();
                }
                con.Close();
            }
        }
    }
}