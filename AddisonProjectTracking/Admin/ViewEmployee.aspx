﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Layout.Master" AutoEventWireup="true" CodeBehind="ViewEmployee.aspx.cs" Inherits="AddisonProjectTracking.ViewEmployee" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <div class="main-content">

            <div class="page-content">
                <div class="container-fluid">

                    <!-- start page title -->
                    <div class="row">
                        <div class="col-12">
                            <div class="page-title-box d-flex align-items-center justify-content-between">
                                <h4 class="mb-0 font-size-18">View Employee</h4>

                                <div class="page-title-right">
                                    <ol class="breadcrumb m-0">
                                        <li class="breadcrumb-item"><a href="javascript: void(0);">Employee Master</a></li>
                                        <li class="breadcrumb-item active">View Employee</li>
                                    </ol>
                                </div>
                                
                            </div>
                        </div>
                    </div>     
                    <!-- end page title -->

                    <div class="row">
                        <div class="col-12">
                             <div class="card">
                                <div class="card-body">
                    
                                    <h4 class="card-title">View Employee</h4>
                                    <p class="card-subtitle mb-4"></p>

                                    <div class="row">
                                        <div class="col-md-6">
                                         
                                               
                                               
                                               
                                                <div class="form-group">
                                                    <label>Employee Code :</label>
                                                    
                                     <asp:Label ID="lblcode" runat="server" Text=""></asp:Label>                 
                                                </div>
                                              <div class="form-group">
                                                    <label>Employee Name :</label>
                                                    
                                     <asp:Label ID="lblname" runat="server" Text=""></asp:Label>                 
                                                </div>
                                            <div class="form-group">
                                                    <label>Department :</label>
                                                    
                                     <asp:Label ID="lbldepart" runat="server" Text=""></asp:Label>                 
                                                </div>
                                            <div class="form-group">
                                                    <label>Reporting Person:</label>
                                                    
                                     <asp:Label ID="lblperson" runat="server" Text=""></asp:Label>                 
                                                </div>
                                             <div class="form-group">
                                                    <label>Email  :</label>
                                                    
                                     <asp:Label ID="lblemail" runat="server" Text=""></asp:Label>                 
                                                </div>
                                       

                                            </div>
                                               <div class="col-md-6">
                                                   <div class="form-group">
                                                    <label>&nbsp;&nbsp;Type  :</label>
                                                    
                                     <asp:Label ID="lbltype" runat="server" Text=""></asp:Label>                 
                                                </div>
                                                    <div class="col-md-6">
                                                   <div class="form-group">
                                                    <label>Mobile:</label>
                                                    
                                     <asp:Label ID="lblmobile" runat="server" Text=""></asp:Label>                 
                                                </div>
                                            <div class="form-group" runat="server" visible="false">
                                                    <label>Updated By :</label>
                                                    
                                     <asp:Label ID="lblupdated" runat="server" Text=""></asp:Label>                 
                                                </div>
                                                             <div class="form-group">
                                                    <label>Created By :</label>
                                                    
                                     <asp:Label ID="lblcreatedby" runat="server" Text=""></asp:Label>                 
                                                </div>
                                                 <div class="form-group">
                                                    <label>Status :</label>
                            <asp:Label ID="lblstatus" runat="server" Text=""></asp:Label>       
                                                </div>
                                                   </div>
                                     </div>
                                    <div class="col-md-12 text center">
                                        <div class="form-group">

                                        <%-- <asp:Button ID="Button1" runat="server" class="btn btn-primary sm" Text="Add Department" OnClick="Button1_Click" />--%>
                                                   <a href="EmployeeList.aspx" class="btn btn-dark btn-sm" style="line-height:1.999">Back</a>
                                               </div>
                                                
                                               
        
                                        
                                       
                                        </div>
                                    </div>
                                 </div>
                            </div>
                        </div>
                    </div>
                </div>
       </div>
    <!-- end col -->
</asp:Content>
