﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.IO;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AddisonProjectTracking.Admin
{
    public partial class EditRequestForChange : System.Web.UI.Page
    {
        string cs = ConfigurationManager.ConnectionStrings["conn"].ConnectionString;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                string request = MyCrypto.GetDecryptedQueryString(Request.QueryString["Id"].ToString());

                SqlConnection con = new SqlConnection(cs);
                con.Open();
                SqlCommand cmd = new SqlCommand("Select RequestforChange,Status,CreatedBy,UpdatedBy from tbl_requestforchange where Id='" + request + "'", con);
                SqlDataReader sdr = cmd.ExecuteReader();
                if (sdr.Read())
                {
                    txtrequset.Text = sdr["RequestforChange"].ToString();


                    ddlStatus.SelectedValue = sdr["Status"].ToString();
                }
                con.Close();
            }
        }
        protected void Button1_Click(object sender, EventArgs e)
        {
            string request = MyCrypto.GetDecryptedQueryString(Request.QueryString["Id"].ToString());

            SqlConnection con = new SqlConnection(cs);

            try
            {
                SqlCommand cmd = new SqlCommand("sp_b_requestforchange", con);

                cmd.CommandType = CommandType.StoredProcedure;
                con.Open();
                cmd.Parameters.AddWithValue("@Id", request);
                cmd.Parameters.AddWithValue("@qtype", "updaterequest");
                cmd.Parameters.AddWithValue("@RequestforChange", txtrequset.Text);
                cmd.Parameters.AddWithValue("@Status", ddlStatus.SelectedItem.Text);
                cmd.Parameters.AddWithValue("@UpdatedOn", DateTime.Now);
                cmd.Parameters.AddWithValue("@UpdatedBy", Session["UserName"].ToString());

                cmd.ExecuteNonQuery();


            }
            catch (Exception ex)
            {
                Response.Write(ex);
            }
            finally
            {
                con.Close();
                Response.Write("<script language='javascript'>alert('Request For Changes Reason Updated Successfully');window.location=('RequestForChangeList.aspx')</script>");
            }
        }
    }
}