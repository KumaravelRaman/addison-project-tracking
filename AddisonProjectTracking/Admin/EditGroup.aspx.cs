﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.IO;
using System.Web.UI;
using System.Web.UI.WebControls;
namespace AddisonProjectTracking.Admin
{
    public partial class EditGroup : System.Web.UI.Page
    {
        string cs = ConfigurationManager.ConnectionStrings["conn"].ConnectionString;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                string division = MyCrypto.GetDecryptedQueryString(Request.QueryString["Id"].ToString());

                SqlConnection con = new SqlConnection(cs);
                con.Open();
                SqlCommand cmd = new SqlCommand("Select Division,Status,CreatedBy,UpdatedBy from tbl_division where Id='" + division + "'", con);
                SqlDataReader sdr = cmd.ExecuteReader();
                if (sdr.Read())
                {
                    txtGroup.Text = sdr["Division"].ToString();


                    ddlStatus.SelectedValue = sdr["Status"].ToString();
                }
                con.Close();
            }
        }
       
        protected void Button1_Click(object sender, EventArgs e)
        {
            string division = MyCrypto.GetDecryptedQueryString(Request.QueryString["Id"].ToString());

            SqlConnection con = new SqlConnection(cs);

            try
            {
                SqlCommand cmd = new SqlCommand("sp_b_division", con);

                cmd.CommandType = CommandType.StoredProcedure;
                con.Open();
                cmd.Parameters.AddWithValue("@Id", division);
                cmd.Parameters.AddWithValue("@qtype", "updatedivision");
                cmd.Parameters.AddWithValue("@division", txtGroup.Text);
                cmd.Parameters.AddWithValue("@Status", ddlStatus.SelectedItem.Text);
                cmd.Parameters.AddWithValue("@UpdatedOn", DateTime.Now);
                cmd.Parameters.AddWithValue("@UpdatedBy", Session["UserName"].ToString());

                cmd.ExecuteNonQuery();


            }
            catch (Exception ex)
            {
                Response.Write(ex);
            }
            finally
            {
                con.Close();
                Response.Write("<script language='javascript'>alert('Division Updated Successfully');window.location=('GroupList.aspx')</script>");
            }
        }
    }
}