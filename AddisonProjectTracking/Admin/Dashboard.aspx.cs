﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web.UI.WebControls;

namespace AddisonProjectTracking.Admin
{
    public partial class Dashboard : System.Web.UI.Page
    {
         string cs = ConfigurationManager.ConnectionStrings["conn"].ConnectionString;
         public string chart = "";
         public string chartproduct = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                getData();
                BindData();
            }
        }
        protected void getData()
        {
            SqlConnection conn = new SqlConnection(cs);
            SqlCommand Cmd = new SqlCommand();
            conn.Open();
            Cmd = new SqlCommand("select count(*) from tbl_employee", conn);
            Int32 count = (Int32)Cmd.ExecuteScalar();

            conn.Close();

            label1.Text = count.ToString();
        }
        protected void BindData()
        {
            SqlConnection conn = new SqlConnection(cs);
            SqlCommand Cmd = new SqlCommand();
            conn.Open();
            Cmd = new SqlCommand("select count(*) from tbl_department", conn);
            Int32 count = (Int32)Cmd.ExecuteScalar();

            conn.Close();

            label2.Text = count.ToString();
        }
    }
}