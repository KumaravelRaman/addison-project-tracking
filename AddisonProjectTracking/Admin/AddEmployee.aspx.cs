﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Web.UI;
using System.Net;
using System.Net.Mail;
using System.Web.UI.WebControls;

namespace AddisonProjectTracking
{
    public partial class AddEmployee : System.Web.UI.Page
    {
        string cs = ConfigurationManager.ConnectionStrings["conn"].ConnectionString;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                getbind();
                reportingperson();
            }

        }
        protected void getbind()
        {
            SqlConnection con = new SqlConnection(cs);
            string com = "Select distinct Id,Department from tbl_department where Department!=''";
            SqlDataAdapter adpt = new SqlDataAdapter(com, con);
            DataTable dt = new DataTable();
            adpt.Fill(dt);
            ddldepartment.DataSource = dt;
            ddldepartment.DataTextField = "Department";
            ddldepartment.DataValueField = "Id";
            ddldepartment.DataBind();
            ddldepartment.Items.Insert(0, new ListItem("-- Select Department --", ""));
        }
        protected void reportingperson()
        {
            SqlConnection con = new SqlConnection(cs);
            string com = "Select distinct Id,EmployeeName from tbl_employee where TypeId!=3";
            SqlDataAdapter adpt = new SqlDataAdapter(com, con);
            DataTable dt = new DataTable();
            adpt.Fill(dt);
            ddl_reportingperson.DataSource = dt;
            ddl_reportingperson.DataTextField = "EmployeeName";
            ddl_reportingperson.DataValueField = "Id";
            ddl_reportingperson.DataBind();
            ddl_reportingperson.Items.Insert(0, new ListItem("-- Select --", ""));
        }
        protected void Button1_Click(object sender, EventArgs e)
        {
            common c = new common();
            SqlConnection con = new SqlConnection(cs);
            try
            {
                SqlCommand cmd = new SqlCommand("sp_b_employee", con);

                cmd.CommandType = CommandType.StoredProcedure;
                con.Open();
                cmd.Parameters.AddWithValue("@qtype", "addemployee");
                cmd.Parameters.AddWithValue("@EmployeeCode", txtCode.Text);
                cmd.Parameters.AddWithValue("@Email", txtEmail.Text);
                cmd.Parameters.AddWithValue("@EmployeeName", txtEmpname.Text);
                cmd.Parameters.AddWithValue("@ReportingPerson", ddl_reportingperson.SelectedItem.Text);
                cmd.Parameters.AddWithValue("@Mobile", txtMobile.Text);
                cmd.Parameters.AddWithValue("@DepartmentId", ddldepartment.SelectedValue);
                cmd.Parameters.AddWithValue("@TypeId", ddlType.SelectedValue);
                cmd.Parameters.AddWithValue("@Status", ddlStatus.SelectedItem.Text);
                cmd.Parameters.AddWithValue("@CreatedOn", DateTime.Now);
                cmd.Parameters.AddWithValue("@CreatedBy", Session["UserName"].ToString());
                cmd.Parameters.AddWithValue("@UserName", txtEmpname.Text);
                cmd.Parameters.AddWithValue("@Password", txtMobile.Text);
                Session["Empname"] = txtEmpname.Text;
                Session["Emppwd"] = txtMobile.Text;
                Session["email"] = txtEmail.Text;
                
                cmd.ExecuteNonQuery();
                string Uname = Session["Empname"].ToString();
                string pwd = Session["Emppwd"].ToString();
                string email = Session["email"].ToString();

                c.mail1(pwd, Uname, email);
            }
            catch (Exception ex)
            {
                Response.Write(ex);
            }
            finally
            {
                con.Close();
                Response.Write("<script language='javascript'>alert('Employee Added Successfully');window.location=('EmployeeList.aspx')</script>");

            }
        }
        public int mail1(string pswd, string user, string mailpass)
        {
            int i = 0;

            try
            {
                DateTime dte = DateTime.Today;
                var dt = dte.ToString("dd/MM/yyyy");
                MailMessage mail = new MailMessage();
                SmtpClient SmtpServer = new SmtpClient();
                mail.From = new MailAddress("mail@brainmagic.info");
                mail.From = new MailAddress("admin@brainmagic.info");
                mail.To.Add(mailpass);
                mail.Subject = "Login credentials"; // Mail Subject
                string body;
                body = "Dear " + user + ", <br/>";
                body += "<br/>Please find below Login details";
                body += "<br/>Your username - " + user + " , password -" + pswd;
                body += "<br/>Thanks & Regards,<br>Addison&Co.Ltd<br>http://www.addison.co.in/";
                mail.Body = body;
                //mail.AlternateViews.Add(Mail_Body());
                mail.IsBodyHtml = true;
                //System.Net.Mail.Attachment attachment;
                //attachment = new System.Net.Mail.Attachment(file); //Attaching File to Mail  
                //mail.Attachments.Add(attachment);
                SmtpServer.Host = "webmail.brainmagic.info"; //PORT  
                SmtpServer.Port = Convert.ToInt32(25); //PORT  

                SmtpServer.DeliveryMethod = SmtpDeliveryMethod.Network;
                SmtpServer.UseDefaultCredentials = false;
                // SmtpServer.Credentials = new NetworkCredential("mail@brainmagic.info", "kp0L2r3!");
                SmtpServer.Credentials = new NetworkCredential("admin@brainmagic.info", "2vdd40#W");
                SmtpServer.Send(mail);
            }
            catch (Exception ex)
            {
                i = 0;
            }
            return i;
        }

        protected void txtEamil_TextChanged(object sender, EventArgs e)
        {
            EmailCheck();
        }
        public void EmailCheck()
        {
            SqlConnection con = new SqlConnection(cs);
            SqlCommand cmd = new SqlCommand("select Email from tbl_employee where Email='"+ txtEmail.Text +"'", con);
            con.Open();
            SqlDataReader dr = cmd.ExecuteReader();
            while (dr.Read())
            {
                if (dr.HasRows == true)
                {
                    // RequiredFieldValidator3.Text = "Employee Email Already Exist";
                    txtEmail.Text = "";
                    Response.Write("<script language='javascript'>alert('Employee email is already exist.')</script>");
                    break;
                }
            }
        }

        protected void txtCode_TextChanged(object sender, EventArgs e)
        {
            EmpCodeCheck();
        }
        public void EmpCodeCheck()
        {
            SqlConnection con = new SqlConnection(cs);
            SqlCommand cmd = new SqlCommand("select EmployeeCode from tbl_employee where EmployeeCode='" + txtCode.Text + "'", con);
            con.Open();
            SqlDataReader dr = cmd.ExecuteReader();
            while (dr.Read())
            {
                if (dr.HasRows == true)
                {
                    // RequiredFieldValidator3.Text = "Employee Email Already Exist";
                    txtCode.Text = "";
                    Response.Write("<script language='javascript'>alert('Employee Code is already exist.')</script>");
                    break;
                }
            }
        }

        protected void txtMobile_TextChanged(object sender, EventArgs e)
        {
            MobileCheck();
        }
        public void MobileCheck()
        {
            SqlConnection con = new SqlConnection(cs);
            SqlCommand cmd = new SqlCommand("select Mobile  from tbl_employee where Mobile='" + txtMobile.Text + "'", con);
            con.Open();
            SqlDataReader dr = cmd.ExecuteReader();
            while (dr.Read())
            {
                if (dr.HasRows == true)
                {
                    // RequiredFieldValidator3.Text = "Employee Email Already Exist";
                    txtMobile.Text = "";
                    Response.Write("<script language='javascript'>alert('Mobile No is already exist.')</script>");
                    break;
                }
            }
        }

    }
}