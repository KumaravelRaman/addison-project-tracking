﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Layout.Master" AutoEventWireup="true" CodeBehind="DepartmentList.aspx.cs" Inherits="AddisonProjectTracking.Admin.DepartmentList" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    <script>
        $(document).ready(function () {
            $("#myInput").on("keyup", function () {
                var value = $(this).val().toLowerCase();
                $("#mytable tr").filter(function () {
                    $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
                });
            });
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="main-content">
        <div class="page-content">
            <div class="container-fluid">
              
                <!-- start page title -->
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">List Of Department</h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Pages</a></li>
                                    <li class="breadcrumb-item active">List Of Department</li>
                                </ol>
                            </div>

                        </div>
                    </div>

                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <%-- <h4 class="card-title">Department List</h4>--%>
                                <div class="row">
                                    <div class="col-md-4">
                                        <a href="AddDepartment.aspx" class="btn btn-success btn-sm">Create New</a>
                                    </div>
                                    <div class="col-md-1 offset-md-4">
                                        Search :
                                    </div>
                                    <div class="col-md-3 pb-2 float-right">
                                        <input class="form-control" id="myInput" type="text" placeholder="Type Something...">
                                    </div>



                                </div>

                                <div class="table-responsive">
                                    <asp:ListView ID="ListView1" runat="server" DataKeyNames="Id" ItemPlaceholderID="itmPlaceholder" OnPagePropertiesChanging="OnPagePropertiesChanging" OnItemDeleting="ListView1_ItemDeleting" OnItemCommand="ListView1_ItemCommand">
                                        <LayoutTemplate>
                                            <table id="basic-datatable" class="table table-hover dt-responsive nowrap">
                                                <thead style="background-color: #346ee0">
                                                    <tr>
                                                        <th class="text-center" style="width: 5%; color: white">S.No</th>
                                                        <th class="text-center" style="width: 25%; color: white">Department Name</th>
                                                        <th class="text-center" style="width: 15%; color: white">Created By</th>

                                                        <th class="text-center" style="width: 15%; color: white">Status</th>
                                                        <th class="text-center" style="width: 20%; color: white">Action</th>
                                                    </tr>
                                                    <tr id="itmPlaceholder" runat="server">
                                                    </tr>
                                                </thead>
                                                <tr>
                                                    <td colspan="2">
                                                        <asp:DataPager ID="dataPagerNumeric" runat="server" PageSize="10">
                                                            <Fields>
                                                                <asp:NextPreviousPagerField FirstPageText="<<" RenderDisabledButtonsAsLabels="true"
                                                                    ButtonType="Button" ShowFirstPageButton="True" ButtonCssClass="btn btn-default"
                                                                    ShowNextPageButton="False" ShowPreviousPageButton="True" Visible="true" />
                                                                <asp:NumericPagerField ButtonCount="5" NumericButtonCssClass="btn btn-default" CurrentPageLabelCssClass="btn btn-success active" />
                                                                <asp:NextPreviousPagerField LastPageText=">>" RenderDisabledButtonsAsLabels="true"
                                                                    ButtonType="Button" ShowLastPageButton="True" ButtonCssClass="btn btn-default"
                                                                    ShowNextPageButton="True" ShowPreviousPageButton="False" Visible="true" />
                                                            </Fields>
                                                        </asp:DataPager>
                                                    </td>
                                                    <td colspan="6" class="number_of_record" style="text-align: right">
                                                        <asp:DataPager ID="dataPageDisplayNumberOfPages" runat="server" PageSize="20">
                                                            <Fields>
                                                                <asp:TemplatePagerField>
                                                                    <PagerTemplate>
                                                                        <span style="color: Black;">Records:
                                                                <%# Container.StartRowIndex >= 0 ? (Container.StartRowIndex + 1) : 0 %>
                                                                -
                                                                <%# (Container.StartRowIndex + Container.PageSize) > Container.TotalRowCount ? Container.TotalRowCount : (Container.StartRowIndex + Container.PageSize)%>
                                                                of
                                                                <%# Container.TotalRowCount %>
                                                                        </span>
                                                                    </PagerTemplate>
                                                                </asp:TemplatePagerField>
                                                            </Fields>
                                                        </asp:DataPager>
                                                    </td>
                                                </tr>

                                            </table>

                                        </LayoutTemplate>
                                        <ItemTemplate>
                                            <tbody id="mytable">
                                                <tr>
                                                    <td class="text-center">
                                                        <asp:Label ID="Label1" runat="server" Text=' <%# DataBinder.Eval(Container.DataItem,"slNo")%>'></asp:Label>

                                                    </td>
                                                    <td class="text-center">
                                                        <asp:Label ID="Label2" runat="server" Text='<%#Eval("Department")%>'></asp:Label>

                                                    </td>
                                                    <td class="text-center">
                                                        <asp:Label ID="Label3" runat="server" Text='<%#Eval("CreatedBy")%>'></asp:Label>

                                                    </td>


                                                    <td class="text-center">
                                                        <asp:Label ID="Label4" runat="server" Text=' <%# db.IsActive(Eval("Status").ToString())%>'></asp:Label>

                                                    </td>
                                                    <td class="text-center">
                                                        <a href="EditDepartment.aspx?Id=<%#  AddisonProjectTracking.MyCrypto.GetEncryptedQueryString( DataBinder.Eval(Container.DataItem,"Id").ToString()) %>" title="Edit Department" data-toggle="tooltip" data-placement="top" class="btn btn-warning btn-sm"><i class="fa fa-edit"></i></a>

                                                        <asp:LinkButton ID="lnkDelete" ToolTip="Delete Department" CommandName="delete" CommandArgument='<%# Eval("Id") %>' class="btn btn-danger btn-sm" data-toggle="tooltip" data-placement="top" runat="server" Text='Delete User' OnClientClick='javascript:return confirm("Are you sure you want to delete?")'><i class="fa fa-trash"></i></asp:LinkButton>
                                                        <a href="ViewDepartment.aspx?Id=<%#  AddisonProjectTracking.MyCrypto.GetEncryptedQueryString( DataBinder.Eval(Container.DataItem,"Id").ToString()) %>" title="View Department" data-toggle="tooltip" data-placement="top" class="btn btn-secondary btn-sm "><i class="fa fa-eye "></i></a>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </ItemTemplate>
                                        <EmptyDataTemplate>
                                            <div class="widget-content">
                                                <div class="table-responsive">
                                                    <table class="table table-hover table-bordered">
                                                        <thead>
                                                            <tr>
                                                                <th class="text-center" style="width: 5%; color: white">S.No</th>
                                                                <th class="text-center" style="width: 25%; color: white">Department Name</th>
                                                                <th class="text-center" style="width: 15%; color: white">Created By</th>

                                                                <th class="text-center" style="width: 15%; color: white">Status</th>
                                                                <th class="text-center" style="width: 20%; color: white">Action</th>

                                                            </tr>
                                                            <tr class="text-center">
                                                                <td colspan="7" style="background-color: white; color: black;">No Records Found</td>
                                                            </tr>

                                                        </thead>
                                                    </table>
                                                </div>
                                            </div>
                                        </EmptyDataTemplate>

                                    </asp:ListView>
                                </div>
                                <!-- end card body-->
                            </div>
                        </div>
                    </div>
                </div>
                <!-- end page title -->


            </div>
        </div>
    </div>


</asp:Content>
