﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Layout.Master" AutoEventWireup="true" CodeBehind="AddEmployee.aspx.cs" Inherits="AddisonProjectTracking.AddEmployee" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="main-content">

        <div class="page-content">
            <div class="container-fluid">

                <!-- start page title -->
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">Add Employees</h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Employee Master</a></li>
                                    <li class="breadcrumb-item active">Add Employees</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>
                <!-- end page title -->

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <%--<h4 class="card-title">Add Employees</h4>--%>
                                <%--<p class="card-subtitle mb-4"></p>--%>
                                <div class="row">
                                    <div class="col-md-6">

                                        <div class="form-group">
                                            <label>Employee Code <span class="text-danger">*</span></label>
                                            <asp:TextBox ID="txtCode" CssClass="form-control" OnTextChanged="txtCode_TextChanged" AutoPostBack="true" placeholder="Enter Code" runat="server"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtCode" Display="Dynamic" ErrorMessage="Enter Code" ForeColor="Red"></asp:RequiredFieldValidator>

                                        </div>

                                        <div class="form-group">
                                            <label>Employee Email <span class="text-danger">*</span></label>
                                            <asp:TextBox ID="txtEmail" OnTextChanged="txtEamil_TextChanged"  AutoPostBack="true" CssClass="form-control" runat="server" TextMode="Email" placeholder="Enter Email"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtEmail" Display="Dynamic" ErrorMessage="Enter Email" ForeColor="Red"></asp:RequiredFieldValidator>

                                        </div>
                                        <div class="form-group">
                                            <label>Mobile No <span class="text-danger">*</span></label>
                                            <asp:TextBox ID="txtMobile" CssClass="form-control" OnTextChanged="txtMobile_TextChanged" AutoPostBack="true" runat="server" placeholder="Enter Mobile no"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtMobile" Display="Dynamic" ErrorMessage="Enter Mobile No" ForeColor="Red"></asp:RequiredFieldValidator>

                                        </div>
                                        <div class="form-group">
                                            <label>Department <span class="text-danger">*</span></label>
                                            <asp:DropDownList ID="ddldepartment" CssClass="form-control" runat="server"></asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="ddldepartment" Display="Dynamic" ErrorMessage="Choose Department" ForeColor="Red"></asp:RequiredFieldValidator>
                                        </div>




                                    </div>
                                    <!-- end col -->

                                    <div class="col-md-6">

                                        <div class="form-group">
                                            <label>Employee Name<span class="text-danger">*</span></label>
                                            <asp:TextBox ID="txtEmpname" CssClass="form-control" runat="server" placeholder="Enter Employee Name"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="txtEmpname" Display="Dynamic" ErrorMessage="Enter Name" ForeColor="Red"></asp:RequiredFieldValidator>
                                        </div>
                                        <div class="form-group">
                                            <label>Reporting Person<span class="text-danger">*</span></label>
                                           <asp:DropDownList runat="server" ID="ddl_reportingperson" CssClass="form-control"></asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="ddl_reportingperson" Display="Dynamic" ErrorMessage="Enter Reporting Person Name" ForeColor="Red"></asp:RequiredFieldValidator>
                                        </div>
                                        <div class="form-group">
                                            <label>Type<span class="text-danger">*</span></label>
                                            <asp:DropDownList ID="ddlType" CssClass="form-control" runat="server">
                                                <asp:ListItem Value="">--Select--</asp:ListItem>
                                                <asp:ListItem Value="1">Director</asp:ListItem>
                                                <asp:ListItem Value="2">Head</asp:ListItem>
                                                <asp:ListItem Value="3">User</asp:ListItem>
                                            </asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ControlToValidate="ddlType" Display="Dynamic" ErrorMessage="Choose Type" ForeColor="Red"></asp:RequiredFieldValidator>
                                        </div>
                                        <div class="form-group">
                                            <label>Status<span class="text-danger">*</span></label>
                                            <asp:DropDownList ID="ddlStatus" CssClass="form-control" runat="server">
                                                <asp:ListItem Value="Active">Active</asp:ListItem>
                                                <asp:ListItem Value="Deactive">Deactive</asp:ListItem>
                                            </asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ControlToValidate="ddlStatus" Display="Dynamic" ErrorMessage="Choose Status" ForeColor="Red"></asp:RequiredFieldValidator>
                                        </div>
                                        

                                    </div>

                                </div>
                                <div class="form-group">

                                    <asp:Button ID="Button1" runat="server" class="btn btn-primary sm"  Text="Submit" OnClick="Button1_Click" />
                                    <a href="EmployeeList.aspx" class="btn btn-dark btn-sm" style="line-height: 1.999">Back</a>
                                </div>
                                <!-- end col -->
                                <!-- end row -->

                                                       
                            </div>
                            <!-- end card-body-->
                        </div>
                        <!-- end card-->
                    </div>
                    <!-- end col -->
                </div>
            </div>
        </div>
    </div>
</asp:Content>
