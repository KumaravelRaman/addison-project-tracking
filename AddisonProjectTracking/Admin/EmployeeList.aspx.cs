﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AddisonProjectTracking.Admin
{
    public partial class EmployeeList : System.Web.UI.Page
    {
        string cs = ConfigurationManager.ConnectionStrings["conn"].ConnectionString;
        public common db = new common();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                dataget();
            }
        }
        protected void dataget()
        {
            SqlConnection con = new SqlConnection(cs);

            SqlCommand cmd = new SqlCommand("sp_b_employee", con);
            cmd.CommandType = CommandType.StoredProcedure;
            con.Open();
            cmd.Parameters.AddWithValue("@qtype", "selectdata");
            SqlDataAdapter sda = new SqlDataAdapter(cmd);
            DataTable ds = new DataTable();
            sda.Fill(ds);
            ListView1.DataSource = ds;

            ListView1.DataBind();
            con.Close();
        }
        public string Id;
        protected void ListView1_ItemCommand(object sender, ListViewCommandEventArgs e)
        {
            if (e.CommandName == "delete")
            {
                Id = e.CommandArgument.ToString();

                deleteFn(Id);
            }


        }
        protected void ListView1_ItemDeleting(object sender, ListViewDeleteEventArgs e)
        {




        }
        protected void OnPagePropertiesChanging(object sender, PagePropertiesChangingEventArgs e)
        {
            (ListView1.FindControl("dataPagerNumeric") as DataPager).SetPageProperties(e.StartRowIndex, e.MaximumRows, false);
            this.dataget(); ;
        }
        protected string deleteFn(string Id)
        {
            SqlConnection con = new SqlConnection(cs);
            string rStatus = "";
            try
            {

                SqlCommand cmd = new SqlCommand("sp_b_employee", con);
                cmd.CommandType = CommandType.StoredProcedure;
                con.Open();
                cmd.Parameters.AddWithValue("@Id", Id);
                cmd.Parameters.AddWithValue("@qtype", "deleteemploye");
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                Response.Write(ex);
            }
            finally
            {
                con.Close();
                Response.Redirect("EmployeeList.aspx");
            }


            return rStatus;
        }

        

    }
}