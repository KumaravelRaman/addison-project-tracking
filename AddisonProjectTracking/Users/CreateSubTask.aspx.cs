﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;
using System.Data.SqlClient;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Collections.Generic;

namespace AddisonProjectTracking.Users
{
    public partial class CreateSubTask : System.Web.UI.Page
    {
        string id = "0";
        DataControl cls = new DataControl();
        SqlConnection Conn = new SqlConnection(ConfigurationManager.ConnectionStrings["conn"].ToString());
        protected void Page_Load(object sender, EventArgs e)
        {
            id = Request.QueryString["id"];
            if (!Page.IsPostBack)
            {
                TaskDept();
                TaskUser();
                TaskPriority();
                TaskStatus();
                TaskGroup();
                TaskSubGroup();
                if (id != "" && id != "0" && id != null)
                {
                    id = cls.Decrypt(Request.QueryString["id"].ToString());
                    bind(id);
                }



            }
        }
            //Create sub task
            public void TaskDept()
            {

                Conn.Open();
                string qry = "select Id,Department from tbl_department where Status='Active'";
                SqlDataAdapter dacname = new SqlDataAdapter(qry, Conn);
                DataSet dsname = new DataSet();
                dacname.Fill(dsname);
                if (dsname.Tables[0].Rows.Count > 0)
                {
                    ddl_dept.DataSource = dsname;
                    ddl_dept.DataTextField = "Department";
                    ddl_dept.DataValueField = "Id";
                    ddl_dept.DataBind();
                    ddl_dept.Items.Insert(0, "Select");

                }
                Conn.Close();
            }
            public void TaskUser()
            {

                Conn.Open();
                string qry = "select Id,EmployeeName from tbl_employee where Status='Active'";
                SqlDataAdapter dacname = new SqlDataAdapter(qry, Conn);
                DataSet dsname = new DataSet();
                dacname.Fill(dsname);
                if (dsname.Tables[0].Rows.Count > 0)
                {
                    ddl_user.DataSource = dsname;
                    ddl_user.DataTextField = "EmployeeName";
                    ddl_user.DataValueField = "Id";
                    ddl_user.DataBind();
                    ddl_user.Items.Insert(0, "Select");

                }
                Conn.Close();
            }
            public void TaskPriority()
            {

                ddl_taskpriority.Items.Insert(0, "Select");
                ddl_taskpriority.Items.Insert(1, "Low");
                ddl_taskpriority.Items.Insert(2, "Medium");
                ddl_taskpriority.Items.Insert(2, "High");
            }
            public void TaskStatus()
            {

                ddl_taskstatus.Items.Insert(0, "Select");
                ddl_taskstatus.Items.Insert(1, "Allocated");
                ddl_taskstatus.Items.Insert(2, "Pending");
                ddl_taskstatus.Items.Insert(2, "Completed");
            }
            public void TaskGroup()
            {

                Conn.Open();
                string qry = "select Id,Division from tbl_division where Status='Active'";
                SqlDataAdapter dacname = new SqlDataAdapter(qry, Conn);
                DataSet dsname = new DataSet();
                dacname.Fill(dsname);
                if (dsname.Tables[0].Rows.Count > 0)
                {
                    ddl_group.DataSource = dsname;
                    ddl_group.DataTextField = "Division";
                    ddl_group.DataValueField = "Id";
                    ddl_group.DataBind();
                    ddl_group.Items.Insert(0, "Select");

                }
                Conn.Close();
            }
            public void TaskSubGroup()
            {

                Conn.Open();
                string qry = "select Id,SubDivision from tbl_subdivision where Status='Active" +
                    "'";
                SqlDataAdapter dacname = new SqlDataAdapter(qry, Conn);
                DataSet dsname = new DataSet();
                dacname.Fill(dsname);
                if (dsname.Tables[0].Rows.Count > 0)
                {
                    ddl_subgroup.DataSource = dsname;
                    ddl_subgroup.DataTextField = "SubDivision";
                    ddl_subgroup.DataValueField = "Id";
                    ddl_subgroup.DataBind();
                    ddl_subgroup.Items.Insert(0, "Select");

                }
                Conn.Close();
            
        }
            public void bind(string id)
            {
                string query = @"select * from tbl_subproject a where Id='" + id + "'";
                DataTable dt = cls.Getdata(query);
                if (dt.Rows.Count > 0)
                {                   
                    lbl_taskname.Text = dt.Rows[0]["TaskName"].ToString();
                    lbl_dept.Text= dt.Rows[0]["Department"].ToString();              
                    lbl_allocateuser.Text= dt.Rows[0]["AllocateUser"].ToString();
                    lbl_group.Text = dt.Rows[0]["TaskGroup"].ToString();
                    lbl_subgroup.Text = dt.Rows[0]["SubGroup"].ToString();
                    lbl_startdate.Text = Convert.ToDateTime(dt.Rows[0]["StartDate"]).ToString("dd/MM/yyyy");
                    lbl_enddate.Text = Convert.ToDateTime(dt.Rows[0]["EndDate"]).ToString("dd/MM/yyyy");
                    lbl_priorty.Text = dt.Rows[0]["Priority"].ToString();
                    lbl_status.Text = dt.Rows[0]["Status"].ToString();                                 
                }
        }

        protected void btn_contactadd_ServerClick(object sender, EventArgs e)
        {
            Cls_taskdetails lclsIsExist = lCls_Productitems.Find(delegate (Cls_taskdetails filedata)
            {
                return filedata.TaskName == (Convert.ToString(txt_subtask.Text));

            });
            if (lclsIsExist == null)
            {
                if (ddl_user.SelectedItem.Text != "Select" && ddl_taskstatus.SelectedItem.Text != "Select" && ddl_taskpriority.SelectedItem.Text != "Select" && ddl_group.SelectedItem.Text != "Select" && ddl_subgroup.SelectedItem.Text != "Select")
                {
                    lclsIsExist = new Cls_taskdetails();
                    lclsIsExist.TaskName = Convert.ToString(txt_subtask.Text);
                    lclsIsExist.DepartmentId = Convert.ToString(ddl_dept.SelectedItem.Text);
                    lclsIsExist.UserId = Convert.ToString(ddl_user.SelectedItem.Text);
                    lclsIsExist.GroupId = Convert.ToString(ddl_group.SelectedItem.Text);
                    lclsIsExist.SubGroupId = Convert.ToString(ddl_subgroup.SelectedItem.Text);                    
                    lclsIsExist.StartDate = Convert.ToDateTime(txt_startdate.Text).Date;
                    lclsIsExist.EndDate = Convert.ToDateTime(txt_enddate.Text).Date;
                    lclsIsExist.Priority = Convert.ToString(ddl_taskpriority.SelectedItem.Text);
                    lclsIsExist.Status = Convert.ToString(ddl_taskstatus.SelectedItem.Text);

                    lCls_Productitems.Add(lclsIsExist);
                    lv_items.DataSource = lCls_Productitems;
                    lv_items.DataBind();

                    //ddl_prod.ClearSelection();
                    ddl_dept.ClearSelection();
                    ddl_user.ClearSelection();
                    ddl_subgroup.ClearSelection();
                    ddl_group.ClearSelection();
                    txt_subtask.Text = "";
                    txt_startdate.Text = "";
                    txt_enddate.Text = "";
                    ddl_taskpriority.ClearSelection();
                    ddl_taskstatus.ClearSelection();

                }

                else
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "alertMessage", "alert('Please select all the column');", true);
                }
            }

        }
        public List<Cls_taskdetails> lCls_Productitems
        {
            get
            {
                if (this.ViewState["productItems"] == null)
                    this.ViewState["productItems"] = new List<Cls_taskdetails>();

                return (
                    List<Cls_taskdetails>)this.ViewState["productItems"];
            }
            set
            {
                this.ViewState["productItem"] = value;
            }
        }
        [Serializable]
        public class Cls_taskdetails
        {
            public string TaskName { get; set; }
            public string DepartmentId { get; set; }
            public string UserId { get; set; }
            public string GroupId { get; set; }
            public string SubGroupId { get; set; }
            public DateTime StartDate { get; set; }
            public DateTime EndDate { get; set; }
            public string Priority { get; set; }
            public string Status { get; set; }
        }

        protected void btn_tasksubmit_Click(object sender, EventArgs e)
        {
            id = cls.Decrypt(Request.QueryString["id"].ToString());
            //id = cls.Decrypt(Request.QueryString["id"].ToString());
            string qryy = @"Update tbl_subproject set IsLeveltwo =@IsLeveltwo,UpdatedOn=@UpdatedOn,UpdatedBy=@UpdatedBy where Id = @Id";
            SqlCommand cmdconn = new SqlCommand(qryy, Conn);
            //Conn.Open();

            cmdconn.Parameters.AddWithValue("@IsLeveltwo", true);
            cmdconn.Parameters.AddWithValue("@CreatedOn", DateTime.Now);
            cmdconn.Parameters.AddWithValue("@Id", id);
            cmdconn.Parameters.AddWithValue("@UpdatedOn", DateTime.Now);
            cmdconn.Parameters.AddWithValue("@UpdatedBy", Session["UserName"]);

            Conn.Open();
            cmdconn.ExecuteNonQuery();
            Conn.Close();
            foreach (RepeaterItem item in lv_items.Items)
            {
                Label TaskName = (Label)item.FindControl("lbl_taskname");
                Label Department = (Label)item.FindControl("lbl_dept");
                Label User = (Label)item.FindControl("lbl_user");
                Label Group = (Label)item.FindControl("lbl_group");
                Label SubGroup = (Label)item.FindControl("lbl_subgroup");
                Label startdate = (Label)item.FindControl("lbl_startdate");
                Label enddate = (Label)item.FindControl("lbl_enddate");
                Label priority = (Label)item.FindControl("lbl_priority");
                Label status = (Label)item.FindControl("lbl_status");

                string qry = @"Insert into tbl_task (ProjectId,TaskId,TaskName,Department,AllocateUser,TaskGroup,SubGroup,StartDate,EndDate,Priority,Status,CreatedOn,CreatedBy,UpdatedOn,UpdatedBy)values
            (@ProjectId,@TaskId,@TaskName,@Department,@AllocateUser,@TaskGroup,@SubGroup,@StartDate,@EndDate,@Priority,@Status,@CreatedOn,@CreatedBy,@UpdatedOn,@UpdatedBy)";
                SqlCommand cmdcon = new SqlCommand(qry, Conn);
                Conn.Open();
                cmdcon.Parameters.AddWithValue("@ProjectId", Session["ProjectId"]);
                cmdcon.Parameters.AddWithValue("@TaskId", id);
                cmdcon.Parameters.AddWithValue("@TaskName", TaskName.Text);
                cmdcon.Parameters.AddWithValue("@Department", Department.Text);
                cmdcon.Parameters.AddWithValue("@AllocateUser", User.Text);
                cmdcon.Parameters.AddWithValue("@TaskGroup", Group.Text);
                cmdcon.Parameters.AddWithValue("@SubGroup", SubGroup.Text);
                cmdcon.Parameters.AddWithValue("@StartDate", Convert.ToDateTime(startdate.Text));
                cmdcon.Parameters.AddWithValue("@EndDate", Convert.ToDateTime(enddate.Text));
                cmdcon.Parameters.AddWithValue("@Priority", priority.Text);                               
                cmdcon.Parameters.AddWithValue("@Status", status.Text);
                cmdcon.Parameters.AddWithValue("@CreatedOn", DateTime.Now);
                cmdcon.Parameters.AddWithValue("@CreatedBy", Session["UserName"]);
                cmdcon.Parameters.AddWithValue("@UpdatedOn", DateTime.Now);
                cmdcon.Parameters.AddWithValue("@UpdatedBy", "");

                //Conn.Open();
                cmdcon.ExecuteNonQuery();
                Conn.Close();
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "usernotfound", "alert('" + "Task Created Successfully" + "'); document.location.href='SearchProject.aspx';", true);

            }
        }
    }
}