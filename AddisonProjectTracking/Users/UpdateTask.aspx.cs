﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;
using System.Data.SqlClient;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Collections.Generic;

namespace AddisonProjectTracking.Users
{
    public partial class UpdateTask : System.Web.UI.Page
    {
      
        DataControl cls = new DataControl();
        SqlConnection Conn = new SqlConnection(ConfigurationManager.ConnectionStrings["conn"].ToString());        
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                id = cls.Decrypt(Request.QueryString["id"].ToString());

                if (Request.QueryString["msg"] != null)
                {
                    string msg = Request.QueryString["msg"].ToString();
                    if (msg == "UpdatedSuccessfully")
                    {
                        Response.Write("<script language='javascript'>alert('Task Updated Successfully');</script>");
                    }
                }

                
                bind(id);
                bind_product();
                btn_milestone.Visible = false;
            }
            
           
        }
        public void bind(string id)
        {
            string query = @"select *,(select PurposeofProject from tbl_purposeofproject p where p.id=a.PurposeOfProject)PurposeOfProjectt,(select Department from tbl_department p where p.id=a.DepartmentId)Department,
(select EmployeeName from tbl_employee p where p.id=a.ProjectOwner)ProjectOwnerr,(select EmployeeName from tbl_employee p where p.id=a.ProjectViewAccess)ProjectViewAccesss,(select ProjectStatus from tbl_projectstatus p where p.id=a.Status)Statuss from tbl_project a where Id='" + id + "'";
            DataTable dt = cls.Getdata(query);
            if (dt.Rows.Count > 0)
            {
                txt_projectname.Text = dt.Rows[0]["ProjectName"].ToString();
                ddl_department.Text = dt.Rows[0]["Department"].ToString();
                ddl_projectowner.Text = dt.Rows[0]["ProjectOwnerr"].ToString();
                ddl_purposre.Text = dt.Rows[0]["PurposeOfProjectt"].ToString();
                ddl_priority.Text = dt.Rows[0]["Priority"].ToString();
                txt_valueofproject.Text = dt.Rows[0]["ValueOfProject"].ToString();
                txt_targetDate.Text = Convert.ToDateTime(dt.Rows[0]["TargetDate"]).ToString("dd/MM/yyyy");
                txt_updateinterval.Text = dt.Rows[0]["UpdateInterval"].ToString();
                ddl_projviewaccess.Text = dt.Rows[0]["ProjectViewAccesss"].ToString();
                ddl_status.Text = dt.Rows[0]["Statuss"].ToString();
                //if (dt.Rows[0]["Status"].ToString() == "2")
                //    Task.Visible = true;
                //else
                //    Task.Visible = false;

            }

        }
        public void bind_product()
        {
            lstv_productdata.DataSource = Search();
            lstv_productdata.DataBind();
        }
        protected DataTable Search()
        {
            string query = string.Empty;
            query = "select * from tbl_subproject  where Projectid="+id+"";
            Session["ProjectId"] = id;
            DataTable dt = cls.Getdata(query);
            return dt;
        }
        public string id;
        protected void lstv_productdata_ItemCommand(object sender, ListViewCommandEventArgs e)
        {
            if (e.CommandName == "subtask")
            {
                id = e.CommandArgument.ToString();
                Response.Redirect("CreateSubTask.aspx?id=" + cls.Encrypt(id));
            }
            if (e.CommandName == "viewtask")
            {
                id = e.CommandArgument.ToString();
                Response.Redirect("UpdateSubTask.aspx?id=" + cls.Encrypt(id));
            }
            if (e.CommandName == "update")
            {
                try
                {
                    string status = Request.Form["ddl_taskstatus"];
                    //id = cls.Decrypt(Request.QueryString["id"].ToString());
                    id = e.CommandArgument.ToString();
                    DropDownList ddl_taskstatus = (DropDownList)e.Item.FindControl("ddl_taskstatus");
                    //DropDownList ddl_taskstatus = (DropDownList)sender;
                    //ListViewDataItem item = ddl_taskstatus.NamingContainer as ListViewDataItem;
                    TextBox txtpercent = (TextBox)e.Item.FindControl("txtpercent");
                    //string qry = @"Update tbl_subproject set Status = @Status, percentageofcompletion = @percentageofcompletion, UpdatedOn = @UpdatedOn, UpdatedBy = @UpdatedBy where Id = @Id";
                    //SqlCommand cmdcon = new SqlCommand(qry, Conn);
                    //Conn.Open();
                    //cmdcon.Parameters.AddWithValue("@Id", id);
                    //cmdcon.Parameters.AddWithValue("@Status", ddl_taskstatus.SelectedValue);
                    //cmdcon.Parameters.AddWithValue("@percentageofcompletion", txtpercent.Text);
                    //cmdcon.Parameters.AddWithValue("@UpdatedOn", DateTime.Now);
                    //cmdcon.Parameters.AddWithValue("@UpdatedBy", Session["UserName"]);
                    Conn.Open();
                    SqlCommand cmd = new SqlCommand("Update tbl_subproject SET Status='" + ddl_taskstatus.SelectedItem.Text + "',percentageofcompletion='" + txtpercent.Text + "',UpdatedOn='" + DateTime.Now + "',UpdatedBy='" + Session["UserName"] + "' where Id='" + id + "'", Conn);
                    cmd.ExecuteNonQuery();
                    Response.Redirect("UpdateTask.aspx?id=" + cls.Encrypt (id) + "");
                   
                }
                catch (Exception ex)
                {
                    Response.Write(ex);
                }
                finally
                {
                    Conn.Close();
                    
                }
            
            }
        }

        protected void ddl_taskstatus_SelectedIndexChanged(object sender, EventArgs e)
        {
            //id = cls.Decrypt(Request.QueryString["id"].ToString());
           

            // DropDownList ddl_taskstatus = (DropDownList)e.Item.FindControl("ddl_taskstatus");

            DropDownList ddl_taskstatus = (DropDownList)sender;
            ListViewDataItem item = ddl_taskstatus.NamingContainer as ListViewDataItem;

            TextBox txtpercent = item.FindControl("txtpercent") as TextBox;


            txtpercent.Visible = true;
            if (ddl_taskstatus.SelectedValue == "1")
            {
                txtpercent.Text = "";
               
                txtpercent.ReadOnly=false;
            }
            else if (ddl_taskstatus.SelectedValue == "2")
            {
                
                txtpercent.Text = "100%";
                txtpercent.ReadOnly=true;
               
            }


            //  }
        }
        protected void CheckBox1_CheckedChanged(object sender, EventArgs e)
        {
            //CheckBox CheckBox1 = (CheckBox)sender;
            //ListViewDataItem item = CheckBox1.NamingContainer as ListViewDataItem;
            //if (CheckBox1.Checked == true)
            //{
            //    btn_milestone.Visible = true;
            //}
            //else
            //{
            //    btn_milestone.Visible = false;
            //}
            // for (int i = 0; i < lstv_productdata.Items.Count; i++)
            //{

            //    CheckBox chk = (CheckBox)lstv_productdata.Items[i].FindControl("CheckBox1");
            //    if (chk.Checked == true)
            //    {
            //        btn_milestone.Visible = true;
            //    }
            //    else
            //    {
            //        btn_milestone.Visible = false;
            //    }

            //}
        }

        protected void btn_milestone_Click(object sender, EventArgs e)
        {
            id = cls.Decrypt(Request.QueryString["id"].ToString());

            //SqlCommand cmd=new SqlCommand("select Id from tblsubproject 
           
        }
    }
}