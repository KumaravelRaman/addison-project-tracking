﻿<%@ Page Title="Search Project" Language="C#" AutoEventWireup="true" MasterPageFile="~/Users/UserLayout.Master" CodeBehind="SearchProject.aspx.cs" Inherits="AddisonProjectTracking.Users.SearchProject" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
         <div class="main-content">

            <div class="page-content">
                <div class="container-fluid">

                    <!-- start page title -->
                    <div class="row">
                        <div class="col-12">
                            <div class="page-title-box d-flex align-items-center justify-content-between">
                                <h4 class="mb-0 font-size-18">List Of Project</h4>

                                <div class="page-title-right">
                                    <ol class="breadcrumb m-0">
                                        <li class="breadcrumb-item"><a href="javascript: void(0);">Pages</a></li>
                                        <li class="breadcrumb-item active">List Of Project</li>
                                    </ol>
                                </div>
                                
                            </div>
                        </div>
                    </div>     
                    <!-- end page title -->

                    <div class="row">
                        <div class="col-12">
                             <div class="card">
                                <div class="card-body">
                                    <h4 class="card-title">Own Project</h4>
                                    <p class="card-subtitle mb-4">
                                       
                                    </p>
<asp:ListView ID="lstv_productdata" runat="server" DataKeyNames="id" ItemPlaceholderID="itmPlaceholder" OnItemCommand="lstv_productdata_ItemCommand" >
                     <LayoutTemplate>
                  
                                <table id="basic-datatable" class="table dt-responsive nowrap">
                                    <thead>
                                        <tr>
                                            <th class="text-center">
                                                Sl No.
                                            </th>
                                            <th class="text-left">
                                               Project Code
                                            </th>  
                                             <th class="text-left">
                                               Project Name
                                            </th> 
                                             <th class="text-left">
                                               Project Owner
                                            </th>  
                                            <th class="text-left">
                                               Raised By
                                            </th>
                                                                                 
                                             <th class="text-left">
                                                Priority
                                            </th>
                                            <th class="text-left">
                                                Target Date
                                            </th>
                                            <th class="text-left">
                                                Status
                                            </th>
                                            <th class="text-center">
                                                Action
                                            </th>
                                            
                                        </tr>
                                        <tr id="itmPlaceholder" runat="server">
                                        </tr>
                                    </thead>
                                            <tr>
                                        <td colspan="2">
                                            <asp:DataPager ID="dataPagerNumeric" runat="server" PageSize="10">
                                                <Fields>
                                                    <asp:NextPreviousPagerField FirstPageText="<<" RenderDisabledButtonsAsLabels="true"
                                                        ButtonType="Button" ShowFirstPageButton="True" ButtonCssClass="btn btn-default"
                                                        ShowNextPageButton="False" ShowPreviousPageButton="True" Visible="true" />
                                                    <asp:NumericPagerField ButtonCount="5" NumericButtonCssClass="btn btn-default" CurrentPageLabelCssClass="btn btn-success active" />
                                                    <asp:NextPreviousPagerField LastPageText=">>" RenderDisabledButtonsAsLabels="true"
                                                        ButtonType="Button" ShowLastPageButton="True" ButtonCssClass="btn btn-default"
                                                        ShowNextPageButton="True" ShowPreviousPageButton="False" Visible="true" />
                                                </Fields>
                                            </asp:DataPager>
                                        </td>
                                        <td colspan="6" class="number_of_record" style="text-align: right">
                                            <asp:DataPager ID="dataPageDisplayNumberOfPages" runat="server" PageSize="10">
                                                <Fields>
                                                    <asp:TemplatePagerField>
                                                        <PagerTemplate>
                                                            <span style="color: Black;">Records:
                                                                <%# Container.StartRowIndex >= 0 ? (Container.StartRowIndex + 1) : 0 %>
                                                                -
                                                                <%# (Container.StartRowIndex + Container.PageSize) > Container.TotalRowCount ? Container.TotalRowCount : (Container.StartRowIndex + Container.PageSize)%>
                                                                of
                                                                <%# Container.TotalRowCount %>
                                                            </span>
                                                        </PagerTemplate>
                                                    </asp:TemplatePagerField>
                                                </Fields>
                                            </asp:DataPager>
                                        </td>
                                    </tr>
                            
                                </table>
                           
                    </LayoutTemplate>
                    <ItemTemplate>
                        <tbody>
                            <tr>
                                <td class="text-center">
                                    <%#Eval("RowNumber")%>
                                </td>
                                <td class="text-left">
                                    <%#Eval("ProjectCode")%>
                                </td>
                                     <td class="text-left">
                                
                                     <%#Eval("ProjectName")%>
                               </td>
                                   <td class="text-left">
                                    <%#Eval("ProjectOwnerr")%>
                                </td>
                                      <td class="text-left">
                                    <%#Eval("RaisedBy")%>
                                </td>                         
                           
                                  <td class="text-left" style="color:blue;font-weight:600;">
                                
                                     <%#Eval("Priority")%>
                                </td>
                                    <td class="text-left">
                                
                                     <%#Eval("TargetDate", "{0:d}")%>
                                </td>
                                     <td class="text-left" style="color:green;font-weight:600;">
                                
                                     <%#Eval("Statuss")%>
                                </td>
                                      <td style="width: 15%; text-align: center;">
                                       
                                        <asp:LinkButton runat="server" ID="c0" CommandName="edit" Text="Edit" Visible='<%# ((int)Eval("Status")==2 || (int)Eval("Status")==1 || (int)Eval("Status")==4) && Session["UserName"].ToString()!="Director"  %>' CommandArgument='<%#Eval("id") %>'
                                            ><i class="fa fa-pen-square" title="Update Project" style="font-size:large;color: #42479b;"></i></asp:LinkButton>&nbsp;
                                       
                                           <asp:LinkButton runat="server" ID="LinkButton1" CommandName="add" Visible='<%# ((int)Eval("Status")!=1 && (int)Eval("Status")!=2 && (int)Eval("Status")!=3 && (int)Eval("Status")!=4 ) && Session["UserName"].ToString()!="Director" %>'  Text="Edit" CommandArgument='<%#Eval("id") %>'
                                            ><i class="fa fa-plus" title="Add Task" style="font-size:large;color: #42479b;"></i></asp:LinkButton>&nbsp;
                                    
                                      <%-- <asp:LinkButton runat="server" ID="LinkButton1" CommandName="delete" Text="Delete" CommandArgument='<%#Eval("id") %>'
                                       ><i title="Delete" class="fa fa-trash" style="font-size:large;    color: #fc1b00;"></i></asp:LinkButton>&nbsp;--%>
                                    <asp:LinkButton runat="server" ID="LinkButton2" CommandName="view" Text="View" CommandArgument='<%#Eval("id") %>'
                                       ><i title="View" class="fa fa-eye" style="font-size:large;color: #228c22;"></i></asp:LinkButton>                                                                        
                                    </td>
                            </tr>
                        </tbody>
                    </ItemTemplate>
                    <EmptyDataTemplate>
                        <div class="widget-content">
                            <div class="table-responsive">
                                <table class="table table-hover table-bordered">
                                    <thead>
                                        <tr>
                                            <th class="text-center">
                                                Sl No.
                                            </th>
                                            <th class="text-center" style="display:none;">
                                               Company Code
                                            </th>  
                                             <th class="text-center">
                                               Manufacturer
                                            </th>  
                                            <th class="text-center">
                                               Part Number 
                                            </th>
                                            <th class="text-center">
                                                OEM Part No
                                            </th>                                       
                                             <th class="text-center">
                                                Product group
                                            </th>
                                            <th class="text-center">
                                                Category
                                            </th>
                                            <th class="text-center">
                                                Part Type
                                            </th>
                                            <th class="text-center">
                                                Action
                                            </th>
                                            
                                        </tr>
                                        <tr class="text-center">
                                            <td  colspan="7" style="background-color: white; color: black;" >No Records Found</td></tr>
                                      
                                    </thead>
                                    </table>
                            </div>
                        </div>
                    </EmptyDataTemplate>
                </asp:ListView>

                                </div> <!-- end card body-->
                            </div> <!-- end card -->
                        </div><!-- end col-->
                    </div>
                    </div>
                </div>
             </div>
    
    </asp:Content>
