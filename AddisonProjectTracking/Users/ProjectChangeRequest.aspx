﻿<%@ Page Title="Project Change Request" Language="C#" MasterPageFile="~/Users/UserLayout.Master" AutoEventWireup="true" CodeBehind="ProjectChangeRequest.aspx.cs" Inherits="AddisonProjectTracking.Users.ProjectChangeRequest" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
         <div class="main-content">

            <div class="page-content">
                <div class="container-fluid">

                    <!-- start page title -->
                    <div class="row">
                        <div class="col-12">
                            <div class="page-title-box d-flex align-items-center justify-content-between">
                                <h4 class="mb-0 font-size-18">Project Change Request</h4>

                                <div class="page-title-right">
                                    <ol class="breadcrumb m-0">
                                        <li class="breadcrumb-item"><a href="javascript: void(0);">Pages</a></li>
                                        <li class="breadcrumb-item active">Project Change Request</li>
                                    </ol>
                                </div>
                                
                            </div>
                        </div>
                    </div>     
                    <!-- end page title -->

                    <div class="row">
                        <div class="col-12">
                             <div class="card">
                                <div class="card-body">
                                    <h4 class="card-title">Own Project</h4>
                                    <p class="card-subtitle mb-4">
                                       
                                    </p>
<asp:ListView ID="lstv_productdata" runat="server" DataKeyNames="id" ItemPlaceholderID="itmPlaceholder" OnItemCommand="lstv_productdata_ItemCommand" OnItemUpdated="lstv_productdata_ItemUpdated">
                     <LayoutTemplate>
                  
                                <table id="basic-datatable" class="table dt-responsive nowrap">
                                    <thead>
                                        <tr>
                                            <th class="text-center">
                                                Sl No.
                                            </th>
                                            <th class="text-left">
                                               Project Code
                                            </th>  
                                             <th class="text-left">
                                               Project Name
                                            </th>  
                                             <th class="text-left">
                                               Project Owner
                                            </th>  
                                                                        
                                              <th class="text-left">
                                               Priority
                                            </th>  
                                            <th class="text-left">
                                                Target Date
                                            </th>
                                            <th class="text-left">
                                                Reason for Change
                                            </th>
                                             <th class="text-left">
                                               Requested Date
                                            </th>
                                            <th class="text-left">
                                                Status
                                            </th>
                                            <th class="text-center">
                                                Action
                                            </th>
                                            
                                        </tr>
                                        <tr id="itmPlaceholder" runat="server">
                                        </tr>
                                    </thead>
                                            <tr>
                                        <td colspan="2">
                                            <asp:DataPager ID="dataPagerNumeric" runat="server" PageSize="10">
                                                <Fields>
                                                    <asp:NextPreviousPagerField FirstPageText="<<" RenderDisabledButtonsAsLabels="true"
                                                        ButtonType="Button" ShowFirstPageButton="True" ButtonCssClass="btn btn-default"
                                                        ShowNextPageButton="False" ShowPreviousPageButton="True" Visible="true" />
                                                    <asp:NumericPagerField ButtonCount="5" NumericButtonCssClass="btn btn-default" CurrentPageLabelCssClass="btn btn-success active" />
                                                    <asp:NextPreviousPagerField LastPageText=">>" RenderDisabledButtonsAsLabels="true"
                                                        ButtonType="Button" ShowLastPageButton="True" ButtonCssClass="btn btn-default"
                                                        ShowNextPageButton="True" ShowPreviousPageButton="False" Visible="true" />
                                                </Fields>
                                            </asp:DataPager>
                                        </td>
                                        <td colspan="6" class="number_of_record" style="text-align: right">
                                            <asp:DataPager ID="dataPageDisplayNumberOfPages" runat="server" PageSize="10">
                                                <Fields>
                                                    <asp:TemplatePagerField>
                                                        <PagerTemplate>
                                                            <span style="color: Black;">Records:
                                                                <%# Container.StartRowIndex >= 0 ? (Container.StartRowIndex + 1) : 0 %>
                                                                -
                                                                <%# (Container.StartRowIndex + Container.PageSize) > Container.TotalRowCount ? Container.TotalRowCount : (Container.StartRowIndex + Container.PageSize)%>
                                                                of
                                                                <%# Container.TotalRowCount %>
                                                            </span>
                                                        </PagerTemplate>
                                                    </asp:TemplatePagerField>
                                                </Fields>
                                            </asp:DataPager>
                                        </td>
                                    </tr>
                            
                                </table>
                           
                    </LayoutTemplate>
                    <ItemTemplate>
                        <tbody>
                            <tr>
                                <td class="text-center">
                                    <%#Eval("RowNumber")%>
                                </td>
                                <td class="text-left">
                                    <%#Eval("ProjectCode")%>
                                </td>
                                 <td class="text-left">
                                
                                     <%#Eval("ProjectName")%>
                               </td>
                                   <td class="text-left">
                                    <%#Eval("ProjectOwnerr")%>
                                </td>
                               
                                 <td class="text-left">
                                    <%#Eval("Priority")%>
                                </td>
                                    <td class="text-left">
                                
                                     <%#Eval("TargetDate", "{0:d}")%>
                                </td>
                                    <td class="text-left">
                                    <%#Eval("Reason")%>
                                </td>
                                  <td class="text-left">
                                
                                     <%#Eval("RequestDate", "{0:d}")%>
                                </td>
                                     <td class="text-left" style="color:green;font-weight:600;">
                                
                                     <%#Eval("Statuss")%>
                                </td>
                                      <td style="width: 15%; text-align: center;">
                                   
                                    <asp:LinkButton CssClass="btn btn-success" runat="server" ID="LinkButton2" CommandName="Approve" Text="Approve" CommandArgument='<%#Eval("id") %>'
                                       ></asp:LinkButton>                                                                        
                                    </td>
                            </tr>
                        </tbody>
                    </ItemTemplate>
                    <EmptyDataTemplate>
                        <div class="widget-content">
                            <div class="table-responsive">
                                <table class="table table-hover table-bordered">
                                    <thead>
                                        <tr>
                                            <th class="text-center">
                                                Sl No.
                                            </th>
                                            <th class="text-center" style="display:none;">
                                               Company Code
                                            </th>  
                                             <th class="text-center">
                                               Manufacturer
                                            </th>  
                                            <th class="text-center">
                                               Part Number 
                                            </th>
                                            <th class="text-center">
                                                OEM Part No
                                            </th>                                       
                                             <th class="text-center">
                                                Product group
                                            </th>
                                            <th class="text-center">
                                                Category
                                            </th>
                                            <th class="text-center">
                                                Part Type
                                            </th>
                                            <th class="text-center">
                                                Action
                                            </th>
                                            
                                        </tr>
                                        <tr class="text-center">
                                            <td  colspan="7" style="background-color: white; color: black;" >No Records Found</td></tr>
                                      
                                    </thead>
                                    </table>
                            </div>
                        </div>
                    </EmptyDataTemplate>
                </asp:ListView>

                                </div> <!-- end card body-->
                            </div> <!-- end card -->
                        </div><!-- end col-->
                    </div>
                    </div>
                </div>
             </div>
    
    </asp:Content>
