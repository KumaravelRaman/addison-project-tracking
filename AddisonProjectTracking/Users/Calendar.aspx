﻿<%@ Page Title="Calendar" Language="C#" AutoEventWireup="true" CodeBehind="Calendar.aspx.cs" Inherits="AddisonProjectTracking.Users.Calendar" %>

<head>
    <meta charset="utf-8" />
    <title>Ubazo - Admin & Dashboard Template</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta content="Premium Multipurpose Admin & Dashboard Template" name="description" />
    <meta content="MyraStudio" name="author" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />

    <!-- App favicon -->
    <link rel="shortcut icon" href="assets/images/favicon.ico">

      <!--calendar css-->
      <link href="../plugins/fullcalendar/css/fullcalendar.min.css" rel="stylesheet" />

    <!-- App css -->
    <link href="../assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/css/icons.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/css/theme.min.css" rel="stylesheet" type="text/css" />

</head>

<body>
     

          
     <a href="Dashboard.aspx">   <div class="topbar-left">
                <div class="text-center">
                    <img src="../images/stock-availability.jpg" style="width: 21%; height: 70px;">
                    <h5 style="margin-left:3%; font-size: 11px; margin-top: -16px; font-style: italic; color: #646464; font-family: Open Sans,sans-serif;">Tools for a changing world</h5>

                </div>
            </div></a>

<div class="main-content" style="margin-left: -1%;margin-top: -6%;">

            <div class="page-content">
                <div class="container-fluid">

                    <!-- start page title -->
                     <!-- start page title -->
                     <div class="row">
                        <div class="col-12">
                            <div class="page-title-box d-flex align-items-center justify-content-between">
                                <h4 class="mb-0 font-size-18">Calendar</h4>

                                <div class="page-title-right">
                                    <ol class="breadcrumb m-0">
                                        <li class="breadcrumb-item"><a href="javascript: void(0);">Pages</a></li>
                                        <li class="breadcrumb-item active">Calendar</li>
                                    </ol>
                                </div>
                                
                            </div>
                        </div>
                    </div>     
                    <!-- end page title -->

                    <div class="row">
                        <div class="col-12">
                            <div class="card m-b-30">
                                <div class="card-body">
    
                                    <div class="row">
                                        <div class="col-lg-3 col-md-4">
    
                                            <h6 class="mb-3">Create Events</h6>
                                            <form method="post" id="add_event_form">
                                                <input type="text" class="form-control new-event-form" placeholder="Create new event..." />
                                            </form>
    
                                            <div id='external-events' class="mt-4">
                                                <h6 class="mb-3">Draggable Events</h6>
                                                <div class='fc-event'>Project Demo</div>
                                                <div class='fc-event'>Project Meeting</div>
                                                <div class='fc-event'>Weekly Review</div>
                                                <div class='fc-event'>Planning</div>
                                                <div class='fc-event'>Kickoff Meeting</div>
                                            </div>
    
                                            <!-- checkbox -->
                                            <div class="custom-control custom-checkbox mt-3">
                                                <input type="checkbox" class="custom-control-input" id="drop-remove" data-parsley-multiple="groups" data-parsley-mincheck="2">
                                                <label class="custom-control-label" for="drop-remove">Remove after drop</label>
                                            </div>
    
                                        </div>
    
                                        <div id='calendar' class="col-lg-9 col-md-8 mt-3 mt-lg-0"></div>
    
                                    </div>
                                    <!-- end row -->
    
                                </div>
                            </div>
                        </div> <!-- end col -->
                    </div> <!-- end row --> 
                </div> <!-- container-fluid -->
            </div>
            <!-- End Page-content -->

            <footer class="footer">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-sm-6">
                            2021 © Addison & Co.Ltd.
                        </div>
                        <div class="col-sm-6">
                            <div class="text-sm-right d-none d-sm-block">
                                   <a href="#"> Developed by Brainmagic Infotech pvt ltd</a>
                            </div>
                        </div>
                    </div>
                </div>
            </footer>

        </div>
              
      
        <!-- Overlay-->
    <div class="menu-overlay"></div>


    <!-- jQuery  -->
    <script src="../assets/js/jquery.min.js"></script>
    <script src="../assets/js/bootstrap.bundle.min.js"></script>
    <script src="../assets/js/metismenu.min.js"></script>
    <script src="../assets/js/waves.js"></script>
    <script src="../assets/js/simplebar.min.js"></script>

     <!--calendar js-->
     <script src="../plugins/jquery-ui/jquery-ui.min.js"></script>
     <script src="../plugins/moment/moment.js"></script>
     <script src='../plugins/fullcalendar/js/fullcalendar.min.js'></script>
     <script src="../assets/pages/calendar-demo.js"></script>

    <!-- App js -->
    <script src="../assets/js/theme.js"></script>
   </body>