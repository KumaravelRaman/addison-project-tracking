﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;
using System.Data.SqlClient;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Collections.Generic;

namespace AddisonProjectTracking.Users
{
    public partial class UpdateSubTask : System.Web.UI.Page
    {
        DataControl cls = new DataControl();
        SqlConnection Conn = new SqlConnection(ConfigurationManager.ConnectionStrings["conn"].ToString());
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                id = cls.Decrypt(Request.QueryString["id"].ToString());

                if (Request.QueryString["msg"] != null)
                {
                    string msg = Request.QueryString["msg"].ToString();
                    if (msg == "UpdatedSuccessfully")
                    {
                        Response.Write("<script language='javascript'>alert('Task Updated Successfully');</script>");
                    }
                }


                bind(id);
                bind_product();
               // btn_milestone.Visible = false;
            }
        }
        public void bind(string id)
        {
            string query = @"select * from tbl_subproject a where Id='" + id + "'";
            DataTable dt = cls.Getdata(query);
            if (dt.Rows.Count > 0)
            {
                lbl_taskname.Text = dt.Rows[0]["TaskName"].ToString();
                lbl_dept.Text = dt.Rows[0]["Department"].ToString();
                lbl_allocateuser.Text = dt.Rows[0]["AllocateUser"].ToString();
                lbl_group.Text = dt.Rows[0]["TaskGroup"].ToString();
                lbl_subgroup.Text = dt.Rows[0]["SubGroup"].ToString();
                lbl_startdate.Text = Convert.ToDateTime(dt.Rows[0]["StartDate"]).ToString("dd/MM/yyyy");
                lbl_enddate.Text = Convert.ToDateTime(dt.Rows[0]["EndDate"]).ToString("dd/MM/yyyy");
                lbl_priorty.Text = dt.Rows[0]["Priority"].ToString();
                lbl_status.Text = dt.Rows[0]["Status"].ToString();
            }

        }
        public void bind_product()
        {
            lstv_productdata.DataSource = Search();
            lstv_productdata.DataBind();
        }
        protected DataTable Search()
        {
            string query = string.Empty;
            query = "select * from tbl_task  where Taskid=" + id + "";
            Session["ProjectId"] = id;
            DataTable dt = cls.Getdata(query);
            return dt;
        }
        public string id;
        protected void lstv_productdata_ItemCommand(object sender, ListViewCommandEventArgs e)
        {
            if (e.CommandName == "subtask")
            {
                id = e.CommandArgument.ToString();
                Response.Redirect("CreateSubTask.aspx?id=" + cls.Encrypt(id));
            }
            if (e.CommandName == "viewtask")
            {
                id = e.CommandArgument.ToString();
                Response.Redirect("UpdateSubTask.aspx?id=" + cls.Encrypt(id));
            }
            if (e.CommandName == "update")
            {
                try
                {
                    string status = Request.Form["ddl_taskstatus"];
                    //id = cls.Decrypt(Request.QueryString["id"].ToString());
                    id = e.CommandArgument.ToString();
                    DropDownList ddl_taskstatus = (DropDownList)e.Item.FindControl("ddl_taskstatus");
                    //DropDownList ddl_taskstatus = (DropDownList)sender;
                    //ListViewDataItem item = ddl_taskstatus.NamingContainer as ListViewDataItem;
                    TextBox txtpercent = (TextBox)e.Item.FindControl("txtpercent");
                    //string qry = @"Update tbl_subproject set Status = @Status, percentageofcompletion = @percentageofcompletion, UpdatedOn = @UpdatedOn, UpdatedBy = @UpdatedBy where Id = @Id";
                    //SqlCommand cmdcon = new SqlCommand(qry, Conn);
                    //Conn.Open();
                    //cmdcon.Parameters.AddWithValue("@Id", id);
                    //cmdcon.Parameters.AddWithValue("@Status", ddl_taskstatus.SelectedValue);
                    //cmdcon.Parameters.AddWithValue("@percentageofcompletion", txtpercent.Text);
                    //cmdcon.Parameters.AddWithValue("@UpdatedOn", DateTime.Now);
                    //cmdcon.Parameters.AddWithValue("@UpdatedBy", Session["UserName"]);
                    Conn.Open();
                    SqlCommand cmd = new SqlCommand("Update tbl_task SET Status='" + ddl_taskstatus.SelectedItem.Text.Trim() + "',percentageofcompletion='" + txtpercent.Text + "',UpdatedOn='" + DateTime.Now + "',UpdatedBy='" + Session["UserName"] + "' where Id='" + id + "'", Conn);
                    cmd.ExecuteNonQuery();
                    Response.Redirect("UpdateSubTask.aspx?id=" + cls.Encrypt(id) + "");

                }
                catch (Exception ex)
                {
                    Response.Write(ex);
                }
                finally
                {
                    Conn.Close();

                }

            }
        }

        protected void ddl_taskstatus_SelectedIndexChanged(object sender, EventArgs e)
        {
            //id = cls.Decrypt(Request.QueryString["id"].ToString());


            // DropDownList ddl_taskstatus = (DropDownList)e.Item.FindControl("ddl_taskstatus");

            DropDownList ddl_taskstatus = (DropDownList)sender;
            ListViewDataItem item = ddl_taskstatus.NamingContainer as ListViewDataItem;

            TextBox txtpercent = item.FindControl("txtpercent") as TextBox;


            txtpercent.Visible = true;
            if (ddl_taskstatus.SelectedValue == "1")
            {
                txtpercent.Text = "";

                txtpercent.ReadOnly = false;
            }
            else if (ddl_taskstatus.SelectedValue == "2")
            {

                txtpercent.Text = "100%";
                txtpercent.ReadOnly = true;

            }


            //  }
        }
    }
}