﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AddisonProjectTracking.Users
{
    public partial class ProjectCode : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //Session["tktno"] = "PJ/2021/0001";
            lbl_projectcode.Text = Session["tktno"].ToString();
        }
    }
}