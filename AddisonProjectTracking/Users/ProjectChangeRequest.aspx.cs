﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.IO;

namespace AddisonProjectTracking.Users
{
    public partial class ProjectChangeRequest : System.Web.UI.Page
    {
        DataControl cls = new DataControl();
        SqlConnection Conn = new SqlConnection(ConfigurationManager.ConnectionStrings["conn"].ToString());
        protected void Page_Load(object sender, EventArgs e)
        {
            bind_product();
        }
        public void bind_product()
        {
            lstv_productdata.DataSource = Search();
            lstv_productdata.DataBind();
        }
        protected DataTable Search()
        {
            string query = string.Empty;

            query = "select ROW_NUMBER() OVER (ORDER BY Id)[RowNumber],*,(select ProjectStatus from tbl_projectStatus b where b.id=a.Status)statuss,(select EmployeeName from tbl_employee c where c.id=a.ProjectOwner)ProjectOwnerr,(select RequestDate from tbl_project_transit)RequestDate," +
                "(select RequestforChange from tbl_requestforchange r where r.id = (select ReasonId from tbl_project_transit))Reason from tbl_project a where(RaisedBy = '" + Session["UserName"] + "'  or ProjectOwner = (select Id from tbl_Employee where EmployeeName = '" + Session["UserName"] + "' ))and Status = 3";

            DataTable dt = cls.Getdata(query);
            return dt;
        }
        public string id;
        protected void lstv_productdata_ItemCommand(object sender, ListViewCommandEventArgs e)
        {
            if (e.CommandName == "Approve")
            {
                id = e.CommandArgument.ToString();                
                string qry = @"Update tbl_project set Status =@Status,UpdatedOn=@UpdatedOn,UpdatedBy=@UpdatedBy where Id = @Id";
                SqlCommand cmdcon = new SqlCommand(qry, Conn);
                //Conn.Open();

                cmdcon.Parameters.AddWithValue("@Status", 4);
                cmdcon.Parameters.AddWithValue("@CreatedOn", DateTime.Now);
                cmdcon.Parameters.AddWithValue("@Id", id);
                cmdcon.Parameters.AddWithValue("@UpdatedOn", DateTime.Now);
                cmdcon.Parameters.AddWithValue("@UpdatedBy", Session["UserName"]);

                Conn.Open();
                cmdcon.ExecuteNonQuery();
                Conn.Close();
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "usernotfound", "alert('" + "Approved Successfully" + "'); document.location.href='SearchProject.aspx';", true);
            }
           
        }

        protected void lstv_productdata_ItemUpdated(object sender, ListViewUpdatedEventArgs e)
        {

        }
    }
}