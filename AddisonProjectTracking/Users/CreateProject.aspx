﻿<%@ Page Title="Create Project" Language="C#" AutoEventWireup="true" MasterPageFile="~/Users/UserLayout.Master" CodeBehind="CreateProject.aspx.cs" Inherits="AddisonProjectTracking.Users.CreateProject" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
        .table {
            width: 140% !important;
        }
    </style>
  
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">   
            <div class="main-content">

            <div class="page-content">
                <div class="container-fluid">
    <div class="row">
                    <div class="col-12">
                         <div class="card">
                            <div class="card-body">
                
                                <h4 class="card-title">New Project</h4>                             
                                <div class="row">
                                    <div class="col-md-6">
                                  
                                            <div class="form-group">
                                                <label>Project Name</label>
                                                <asp:TextBox runat="server" CssClass="form-control" ID="txt_projectname"></asp:TextBox>
                                                <asp:RequiredFieldValidator runat="server" ControlToValidate="txt_projectname" ErrorMessage="This Field is required" ForeColor="Red"></asp:RequiredFieldValidator>
                                            </div>
                                            <div class="form-group">
                                                <label>Purpose of Project</label>
                                              <asp:DropDownList runat="server" ID="ddl_purposre" CssClass="form-control"></asp:DropDownList>
                                              <asp:RequiredFieldValidator runat="server" ControlToValidate="ddl_purposre" ErrorMessage="This Field is required" ForeColor="Red"></asp:RequiredFieldValidator>
                                            </div>
                                            <div class="form-group">
                                                <label>Department</label>
                                                <asp:DropDownList runat="server" ID="ddl_department" CssClass="form-control" OnSelectedIndexChanged="ddl_department_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                                              <asp:RequiredFieldValidator runat="server" ControlToValidate="ddl_department" ErrorMessage="This Field is required" ForeColor="Red"></asp:RequiredFieldValidator>
                                            </div>
                                            <div class="form-group">
                                                <label>Project Owner</label>
                                                 <asp:DropDownList runat="server" ID="ddl_projectowner" CssClass="form-control"></asp:DropDownList>
                                              <asp:RequiredFieldValidator runat="server" ControlToValidate="ddl_projectowner" ErrorMessage="This Field is required" ForeColor="Red"></asp:RequiredFieldValidator>
                                            </div>
                                            <div class="form-group m-b-0">
                                                <label>Priority</label>
                                                <asp:DropDownList runat="server" ID="ddl_priority" CssClass="form-control"></asp:DropDownList>
                                              <asp:RequiredFieldValidator runat="server" ControlToValidate="ddl_priority" ErrorMessage="This Field is required" ForeColor="Red"></asp:RequiredFieldValidator>
                                            </div>
                                         <div class="form-group m-b-0">
                                                <label>Upload File</label>
                                               <asp:FileUpload runat="server" ID="fileupload" />
                                            </div>
                                             <div class="form-group m-b-0" id="reason" runat="server" visible="false">
                                                <label>Request for Change </label>
                                                  <asp:DropDownList runat="server" ID="ddl_requestforchange" CssClass="form-control"></asp:DropDownList>
                                              <%--<asp:RequiredFieldValidator runat="server" ControlToValidate="txt_requesteddate" ErrorMessage="This Field is required" ForeColor="Red"></asp:RequiredFieldValidator>--%>
                                            </div>
                                    </div> <!-- end col -->

                                    <div class="col-md-6">
                                      
                                            <div class="form-group">
                                                <label>Value of Project</label>
                                                <asp:TextBox runat="server" CssClass="form-control" ID="txt_valueofproject"></asp:TextBox>
                                                <asp:RequiredFieldValidator runat="server" ControlToValidate="txt_valueofproject" ErrorMessage="This Field is required" ForeColor="Red"></asp:RequiredFieldValidator>
                                            </div>
                                            <div class="form-group">
                                                <label>Target Date</label>
                                               <asp:TextBox runat="server" CssClass="form-control" ID="txt_targetDate" type="Date"></asp:TextBox>
                                              <asp:RequiredFieldValidator runat="server" ControlToValidate="ddl_priority" ErrorMessage="This Field is required" ForeColor="Red"></asp:RequiredFieldValidator>
                                            </div>
                                            <div class="form-group">
                                                <label>Task Update Interval</label>
                                      <asp:TextBox runat="server" CssClass="form-control" ID="txt_updateinterval" type="Number"></asp:TextBox>
                                              <asp:RequiredFieldValidator runat="server" ControlToValidate="txt_updateinterval" ErrorMessage="This Field is required" ForeColor="Red"></asp:RequiredFieldValidator>
                                            </div>

                                            <div class="form-group">
                                                <label>Project View Access</label>
                                                <asp:DropDownList runat="server" ID="ddl_projviewaccess" CssClass="form-control"></asp:DropDownList>
                                              <asp:RequiredFieldValidator runat="server" ControlToValidate="ddl_projviewaccess" ErrorMessage="This Field is required" ForeColor="Red"></asp:RequiredFieldValidator>
                                            </div>

                                            <div class="form-group m-b-0">
                                                <label>Status</label>
                                                  <asp:DropDownList runat="server" ID="ddl_status" OnSelectedIndexChanged="ddl_status_SelectedIndexChanged" AutoPostBack="true" CssClass="form-control"></asp:DropDownList>
                                              <asp:RequiredFieldValidator runat="server" ControlToValidate="ddl_status" ErrorMessage="This Field is required" ForeColor="Red"></asp:RequiredFieldValidator>
                                            </div>
                                        <div class="form-group m-b-0" id="reject" runat="server" visible="false">
                                                <label>Requested Date</label>
                                                  <asp:TextBox runat="server" ID="txt_requesteddate" CssClass="form-control" type="Date"></asp:TextBox>
                                              <%--<asp:RequiredFieldValidator runat="server" ControlToValidate="txt_requesteddate" ErrorMessage="This Field is required" ForeColor="Red"></asp:RequiredFieldValidator>--%>
                                            </div>
                                         <div class="form-group m-b-0">
                                               
                                             <asp:Button runat="server" ID="btn_submit" OnClick="btn_submit_Click" class="btn btn-success waves-effect waves-light" Text="Create Project"  />
                                              <asp:Button runat="server" ID="btn_update" OnClick="btn_update_Click" CssClass ="btn btn-success waves-effect waves-light" Text="Update" CausesValidation="false" />
                                            </div>
                                       
                                    </div> <!-- end col -->
                                </div>
                                <!-- end row -->

                            </div> <!-- end card-body-->
                        </div> <!-- end card-->
                    </div> <!-- end col -->
                </div>
                        <div class="row" id="Task" runat="server" visible="false">
                    <div class="col-12">
                         <div class="card">
                            <div class="card-body" style="overflow: auto;">
                
                                <h4 class="card-title">Create Sub Project</h4>  
                                <table id="basic-datatable" class="table dt-responsive nowrap">
                            <thead>
                                <tr>
                                            <th class="text-center">Task Name
                                            </th>
                                            <th class="text-center">Department
                                            </th>
                                            <th class="text-center">User
                                            </th>
                                            <th class="text-center">Group
                                            </th>
                                            <th class="text-center">Sub Group                                                
                                            </th>  
                                            <th class="text-center">Start Date                                                
                                            </th>  
                                            <th class="text-center">End Date                                              
                                            </th> 
                                            <th class="text-center">Priority                                              
                                            </th>                                            
                                            <th class="text-center">Status                                             
                                            </th> 
                                            <th class="text-center">Action
                                            </th>
                                        <%--    <th>Create Sub Task
                                            </th>
                                            <th>Create Milestone
                                            </th>--%>
                                        </tr>
                                </thead>
                            <tbody>
                                <tr style="background-color: White">
                                            <td>
                                                <div>
                                                    <div class="col-md-12">                                                       
                                                        <asp:TextBox runat="server" ID="txt_subtask" CssClass="form-control"></asp:TextBox>
                                                    </div>                                                  
                                                </div>
                                            </td>
                                              
                     
                                                <td>
                                                     <div class="form-group">
                                                          <div class="input-group col-md-12">
                                                                  <asp:DropDownList runat="server" ID="ddl_dept" CssClass="form-control" OnSelectedIndexChanged="ddl_dept_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                                                          </div>
                                                    </div>
                                                </td>
                 
                                            <td>
                                                <div class="form-group ">
                                                    <div class="input-group col-md-12">
                                                          <asp:DropDownList runat="server" ID="ddl_user" CssClass="form-control"></asp:DropDownList>
                                                    </div>                                                  
                                                </div>
                                            </td>
                                            <td>
                                                <div class="form-group ">
                                                    <div class="input-group col-md-12">
                                                      <asp:DropDownList runat="server" ID="ddl_group" CssClass="form-control" OnSelectedIndexChanged="ddl_group_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                                                    </div>
                                                    <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator12" runat="server" ErrorMessage="*" ForeColor="Red"  ValidationGroup="addvalid"
                                                        ControlToValidate="ddl_rat3" SetFocusOnError="true" Display="Dynamic" InitialValue="0"></asp:RequiredFieldValidator>--%>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="form-group ">
                                                    <div class="input-group col-md-12">
                                                           <asp:DropDownList runat="server" ID="ddl_subgroup" CssClass="form-control"></asp:DropDownList>
                                                    </div>

                                                </div>
                                            </td>  
                                      <td>
                                                <div class="form-group ">
                                                    <div class="input-group col-md-12">
                                                      <asp:TextBox runat="server" ID="txt_startdate" CssClass="form-control" type="date"></asp:TextBox>
                                                    </div>
                                                 
                                                </div>
                                            </td>
                                            <td>
                                                <div class="form-group ">
                                                    <div class="input-group col-md-12">
                                                          <asp:TextBox runat="server" ID="txt_enddate" CssClass="form-control" type="date"></asp:TextBox>
                                                    </div>

                                                </div>
                                            </td>    
                                      <td>
                                                <div class="form-group ">
                                                    <div class="input-group col-md-12">
                                                      <asp:DropDownList runat="server" ID="ddl_taskpriority" CssClass="form-control"></asp:DropDownList>
                                                    </div>                                                
                                                </div>
                                            </td>
                                            <td>
                                                <div class="form-group ">
                                                    <div class="input-group col-md-12">
                                                           <asp:DropDownList runat="server" ID="ddl_taskstatus" CssClass="form-control"></asp:DropDownList>
                                                    </div>

                                                </div>
                                            </td>    
                                            <td>
                                                <div class="form-group has-success">
                                                    <button class="btn grp" causesvalidation="false" style="background-color: #FC1B00;border-color: #FC1B00;color: #fff;font-family:Arial;" type="button" runat="server" id="btn_contactadd" onserverclick="btn_contactadd_ServerClick" >
                                                        &nbsp;Add
                                                    </button>
                                                </div>
                                            </td>
                                  <%--   <td>
                                                <div class="form-group has-success">
                                                    <button class="btn grp" style="background-color: #FC1B00;border-color: #FC1B00;color: #fff;font-family:Arial;" type="button" runat="server" id="Button1" onserverclick="btn_contact1_ServerClick" >
                                                        &nbsp;Add sub Task
                                                    </button>
                                                </div>
                                            </td>
                                     <td>
                                                <div class="form-group has-success">
                                                    <button class="btn grp" style="background-color: #FC1B00;border-color: #FC1B00;color: #fff;font-family:Arial;" type="button" runat="server" id="Button2" onserverclick="btn_contact2_ServerClick" >
                                                        &nbsp;Add Milestone
                                                    </button>
                                                </div>
                                            </td>--%>
                                        </tr>
                            </tbody>
                                 <asp:Panel ID="pnl_productdetails" runat="server">
                                    <table class="table dt-responsive nowrap">
                                        <asp:Repeater ID="lv_items" runat="server" >
                                            <HeaderTemplate>
                                        <tr>
                                          <th>Task Name
                                            </th>
                                            <th>Department
                                            </th>
                                            <th>User
                                            </th>
                                            <th>Group
                                            </th>
                                            <th>Sub Group                                                
                                            </th>  
                                            <th>Start Date                                                
                                            </th>  
                                            <th>End Date                                              
                                            </th> 
                                            <th>Priority                                              
                                            </th>                                            
                                            <th>Status                                             
                                            </th> 
                                          
                                            <%--<th>Create Sub Task
                                            </th>
                                            <th>Create Milestone
                                            </th>--%>
                                        </tr>
                                                </HeaderTemplate>
                                            <ItemTemplate>
                                        <tr>
                                            <td>
                                                <div>
                                                    <div>
                                                          <asp:Label ID="lbl_taskname" runat="server" Text='<%# Eval("TaskName")%>'></asp:Label>
                                                    </div>
                                                </div>
                                            </td>
                                              
                     
                                                <td>
                                                     <div>
                                                          <div>
                                                                 <asp:Label ID="lbl_dept" runat="server" Text='<%# Eval("DepartmentId")%>'></asp:Label>
                                                          </div>
                                                    </div>
                                                </td>
                 
                                            <td>
                                                <div>
                                                    <div>
                                                         <asp:Label ID="lbl_user" runat="server" Text='<%# Eval("UserId")%>'></asp:Label>
                                                        
                                                    </div>
                                                   
                                                </div>
                                            </td>
                                            <td>
                                                <div>
                                                    <div>
                                                         <asp:Label ID="lbl_group" runat="server" Text='<%# Eval("GroupId")%>'></asp:Label>
                                                    </div>
                                                    
                                                </div>
                                            </td>
                                            <td>
                                                <div>
                                                    <div>
                                                       <asp:Label ID="lbl_subgroup" runat="server" Text='<%# Eval("SubGroupId")%>'></asp:Label>
                                                    </div>

                                                </div>
                                            </td>    
                                             <td>
                                                <div>
                                                    <div>
                                                         <asp:Label ID="lbl_startdate" runat="server" Text='<%# Eval("StartDate")%>'></asp:Label>
                                                    </div>
                                                    
                                                </div>
                                            </td>
                                            <td>
                                                <div>
                                                    <div>
                                                       <asp:Label ID="lbl_enddate" runat="server" Text='<%# Eval("EndDate")%>'></asp:Label>
                                                    </div>

                                                </div>
                                            </td>    
                                             <td>
                                                <div>
                                                    <div>
                                                       <asp:Label ID="lbl_priority" runat="server" Text='<%# Eval("Priority")%>'></asp:Label>
                                                    </div>

                                                </div>
                                            </td>  
                                              <td>
                                                <div>
                                                    <div>
                                                       <asp:Label ID="lbl_status" runat="server" Text='<%# Eval("Status")%>'></asp:Label>
                                                    </div>

                                                </div>
                                            </td>  
                                        </tr>
                                        </ItemTemplate>
                                            <FooterTemplate>
                                            </FooterTemplate>
                                        </asp:Repeater>
                                    </table>
                                </asp:Panel>


                                
                      
                                
                            
                            
                        </table>
                                   <div class="form-group m-b-0 text-center">
                                               
                                             <asp:Button runat="server" ID="btn_tasksubmit" OnClick="btn_tasksubmit_Click" class="btn btn-success waves-effect waves-light" Text="Submit" />
                                            </div>
                                </div>
                             </div>
                        </div>
                            </div>


                    </div>
                </div>
                </div>
    </asp:Content>