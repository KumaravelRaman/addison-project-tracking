﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Users/UserLayout.Master" CodeBehind="ProjectCode.aspx.cs" Inherits="AddisonProjectTracking.Users.ProjectCode" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

  <style>
   
  </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">   
   
                <div class="container">
<div class="row">
                <div class="col-12">
                    <div class="d-flex align-items-center min-vh-100">
                        <div class="w-100 d-block my-5">
                            <div class="row justify-content-center">
                                <div class="col-md-8 col-lg-5">
                                     <div class="card card-animate">
                                        <div class="card-body">
                                            <div class="text-center mb-4 mt-3">
                                                <a href="index.html">
                                                    <%--<span><img src="assets/images/logo-dark.png" alt="" height="26"></span>--%>
                                                </a>
                                            </div>
                                            <div class="mt-4 pt-3 text-center">
                                                <div class="row justify-content-center">
                                                    <div class="col-6 my-4">
                                                        <img src="../Images/th.jpg" />
                                                    </div>
                                                </div>
                                                <h3 class="expired-title mb-4 mt-3">Project Created Successfully</h3>
                                                   <h4 class="text-muted mb-4 w-75 m-auto">Project Code: <asp:Label runat="server" ID="lbl_projectcode"></asp:Label></h4>
                                            </div>
            
                                            <div class="mb-3 mt-4 text-center">
                                                <a href="" class="btn btn-success"><i class="fa fa-train"></i> Track Project</a>
                                            </div>
                                        </div>
                                        <!-- end card-body -->
                                    </div>
                                    <!-- end card -->
            
                                </div>
                                <!-- end col -->
                            </div>
                            <!-- end row -->
                        </div> <!-- end .w-100 -->
                    </div> <!-- end .d-flex -->
                </div> <!-- end col-->
            </div>
                    </div>
         
    </asp:Content>
