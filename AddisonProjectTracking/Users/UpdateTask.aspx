﻿<%@ Page Title="Update Task" Language="C#" AutoEventWireup="true" MasterPageFile="~/Users/UserLayout.Master" CodeBehind="UpdateTask.aspx.cs" Inherits="AddisonProjectTracking.Users.UpdateTask" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
    .table {
            width: 132% !important;
        }
        </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="main-content">

        <div class="page-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">

                                <h4 class="card-title">Project Details</h4>
                                <div class="row">
                                    <div class="col-md-6">

                                        <div class="form-group">
                                            <label class="col-md-3">Project Name:</label>
                                            <asp:Label runat="server" ID="txt_projectname" CssClass="col-md-3"></asp:Label>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3">Purpose of Project:</label>
                                            <asp:Label runat="server" ID="ddl_purposre" class="col-md-3"></asp:Label>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3">Department:</label>
                                            <asp:Label runat="server" ID="ddl_department" class="col-md-3"></asp:Label>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3">Project Owner:</label>
                                            <asp:Label runat="server" ID="ddl_projectowner" class="col-md-3"></asp:Label>
                                        </div>
                                        <div class="form-group m-b-0">
                                            <label class="col-md-3">Priority:</label>
                                            <asp:Label runat="server" ID="ddl_priority" class="col-md-3"></asp:Label>
                                        </div>
                                        <div class="form-group m-b-0">
                                            <label class="col-md-3">Upload File:</label>
                                            <a href="#" class="col-md-3"><i class="fa fa-eye" style="color: blue;"></i></a>
                                        </div>

                                    </div>
                                    <!-- end col -->

                                    <div class="col-md-6">

                                        <div class="form-group">
                                            <label class="col-md-3">Value of Project:</label>
                                            <asp:Label runat="server" ID="txt_valueofproject" class="col-md-3"></asp:Label>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3">Target Date:</label>
                                            <asp:Label runat="server" ID="txt_targetDate" class="col-md-3"></asp:Label>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3">Task Update Interval:</label>
                                            <asp:Label runat="server" ID="txt_updateinterval" class="col-md-3"></asp:Label>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-md-3">Project View Access:</label>
                                            <asp:Label runat="server" ID="ddl_projviewaccess" class="col-md-3"></asp:Label>
                                        </div>

                                        <div class="form-group m-b-0">
                                            <label class="col-md-3">Status:</label>
                                            <asp:Label runat="server" ID="ddl_status" class="col-md-3"></asp:Label>
                                        </div>
                                        <div class="form-group m-b-0" id="reject" runat="server" visible="false">
                                            <label class="col-md-3">Requested Date:</label>
                                            <asp:Label runat="server" ID="txt_requesteddate" class="col-md-3"></asp:Label>
                                        </div>


                                    </div>
                                    <!-- end col -->
                                </div>
                                <!-- end row -->

                            </div>
                            <!-- end card-body-->
                        </div>
                        <!-- end card-->
                    </div>
                    <!-- end col -->
                </div>
                <div class="row" id="Task" runat="server">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body"  style="overflow: auto;">

                                <h4 class="card-title">Task Details</h4>
                                <asp:ListView ID="lstv_productdata" runat="server" DataKeyNames="id" ItemPlaceholderID="itmPlaceholder" OnItemCommand="lstv_productdata_ItemCommand">
                                    <LayoutTemplate>

                                        <table id="basic-datatable" class="table dt-responsive nowrap">
                                            <thead>
                                                <tr>
                                                    <tr>
                                                        <th>Check
                                                        </th>
                                                        <th>Task Name
                                                        </th>

                                                        <th>Department
                                                        </th>
                                                        <th>User
                                                        </th>
                                                        <th>Group
                                                        </th>
                                                        <th>Sub Group                                                
                                                        </th>
                                                        <th>Start Date                                                
                                                        </th>
                                                        <th>End Date                                              
                                                        </th>
                                                        <th>Priority                                              
                                                        </th>
                                                        <th>Status                                             
                                                        </th>
                                                        <th>Percent
                                                        </th>
                                                        <th class="text-center">Action
                                                        </th>
                                                        <th class="text-center">Create Sub Task
                                                        </th>
                                                    </tr>

                                                </tr>
                                                <tr id="itmPlaceholder" runat="server">
                                                </tr>
                                            </thead>


                                        </table>

                                    </LayoutTemplate>
                                    <ItemTemplate>
                                        <tbody>
                                            <tr>
                                                <td class="text-center">
                                                    <asp:CheckBox ID="CheckBox1" AutoPostBack="true" runat="server"
                                                        OnCheckedChanged="CheckBox1_CheckedChanged" />

                                                </td>
                                                <td class="text-center">
                                                    <%#Eval("TaskName")%>
                                                </td>
                                                <td class="text-left">
                                                    <%#Eval("Department")%>
                                                </td>
                                                <td class="text-left">
                                                    <%#Eval("AllocateUser")%>
                                                </td>
                                                <td class="text-left">
                                                    <%#Eval("TaskGroup")%>
                                                </td>
                                                <td class="text-left">

                                                    <%#Eval("SubGroup")%>
                                                </td>
                                                <td class="text-left" style="color: blue; font-weight: 600;">

                                                    <%#Eval("StartDate", "{0:d}")%>
                                                </td>
                                                <td class="text-left">

                                                    <%#Eval("EndDate", "{0:d}")%>
                                                </td>
                                                <td class="text-left" style="color: blue; font-weight: 600;">

                                                    <%#Eval("Priority")%>
                                                </td>
                                                </td>
                                     <td class="text-left" style="color: green; font-weight: 600;">
                                         <asp:DropDownList CssClass="form-control" runat="server" OnSelectedIndexChanged="ddl_taskstatus_SelectedIndexChanged" ID="ddl_taskstatus" AutoPostBack="true"  Visible='<%# (string)Eval("Status")=="Allocated" && (string)Eval("AllocateUser")==Session["UserName"].ToString() %>'>
                                             <asp:ListItem Value="0"> Select</asp:ListItem>
                                             <asp:ListItem Value="1"> Pending</asp:ListItem>
                                             <asp:ListItem Value="2"> Completed</asp:ListItem>
                                         </asp:DropDownList>

                                         <asp:Label runat="server" Visible='<%# (string)Eval("Status")=="Completed" %>' Text="Completed"></asp:Label>

                                     </td>
                                                <td>
                                                    <asp:TextBox ID="txtpercent" Visible="false" runat="server" CssClass="form-control" Style="width:50%"></asp:TextBox>
                                                     <asp:Label runat="server" Visible='<%# (string)Eval("Status")=="Completed" %>' Text="100%"></asp:Label>
                                                </td>
                                                <td class="text-center">
                                                     <asp:Label runat="server" Visible='<%# (string)Eval("Status")=="Completed" %>' Text="-"></asp:Label>
                                                    <asp:LinkButton runat="server" CssClass="btn btn-success" ID="c0" Visible='<%# (string)Eval("Status")=="Pending" || (string)Eval("Status")=="Allocated" && (string)Eval("AllocateUser")==Session["UserName"].ToString() %>' CommandName="update" Text="Edit" CommandArgument='<%#Eval("id") %>'>Update</asp:LinkButton>&nbsp;</td>
                                                <td class="text-center">
                                                     <asp:Label runat="server" Visible='<%# (string)Eval("Status")=="Completed" %>' Text="-"></asp:Label>
                                                    <asp:LinkButton runat="server" CssClass="btn btn-dark" ID="LinkButton1" CommandName="subtask" Text="Edit" Visible='<%# (string)Eval("Status")=="Pending" || (string)Eval("Status")=="Allocated" && (bool)Eval("IsLeveltwo")==false && (string)Eval("AllocateUser")==Session["UserName"].ToString()  %>' CommandArgument='<%#Eval("id") %>'>Add</asp:LinkButton>&nbsp;
                                                     <asp:LinkButton runat="server" CssClass="btn btn-dark" ID="LinkButton2" CommandName="viewtask" Text="Edit" Visible='<%#(bool)Eval("IsLeveltwo")==true%>' CommandArgument='<%#Eval("id") %>'>View</asp:LinkButton>&nbsp;</td>
                                                </td>

                                            </tr>
                                        </tbody>
                                    </ItemTemplate>
                                    <EmptyDataTemplate>
                                        <div class="widget-content">
                                            <div class="table-responsive">
                                                <table class="table table-hover table-bordered">
                                                    <thead>
                                                        <tr>
                                                            <th class="text-center">Sl No.
                                                            </th>
                                                            <th>check
                                                            </th>
                                                            <th class="text-center" style="display: none;">Company Code
                                                            </th>
                                                            <th class="text-center">Manufacturer
                                                            </th>
                                                            <th class="text-center">Part Number 
                                                            </th>
                                                            <th class="text-center">OEM Part No
                                                            </th>
                                                            <th class="text-center">Product group
                                                            </th>
                                                            <th class="text-center">Category
                                                            </th>
                                                            <th class="text-center">Part Type
                                                            </th>
                                                            <th class="text-center">Action
                                                            </th>
                                                        </tr>
                                                        <tr class="text-center">
                                                            <td colspan="7" style="background-color: white; color: black;">No Records Found</td>
                                                        </tr>

                                                    </thead>
                                                </table>
                                            </div>
                                        </div>
                                    </EmptyDataTemplate>
                                </asp:ListView>
                            </div>

                            <div class="text-center">
                                <asp:Button ID="btn_milestone" runat="server" Text="Submit" CssClass="btn btn-primary"  OnClick="btn_milestone_Click" />
                            </div>
                            
                        </div>
                    </div>
                </div>


            </div>
        </div>
    </div>
</asp:Content>
