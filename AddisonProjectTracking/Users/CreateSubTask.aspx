﻿<%@ Page Title="Create Sub Task" Language="C#" AutoEventWireup="true" MasterPageFile="~/Users/UserLayout.Master" CodeBehind="CreateSubTask.aspx.cs" Inherits="AddisonProjectTracking.Users.CreateSubTask" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
      <style>
    .table {
            width: 140% !important;
        }
        </style>
  
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">   
            <div class="main-content">

            <div class="page-content">
                <div class="container-fluid">
    <div class="row">
                    <div class="col-12">
                         <div class="card">
                            <div class="card-body">
                
                                <h4 class="card-title">Task Details</h4>                             
                                <div class="row">
                                    <div class="col-md-6">
                                  
                                            <div class="form-group">
                                                <label class="col-md-3">Task Name:</label>
                                                <asp:Label runat="server" ID="lbl_taskname" CssClass="col-md-3"></asp:Label>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-3">Department:</label>
                                                <asp:Label runat="server" ID="lbl_dept" class="col-md-3"></asp:Label>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-3">Allocated User</label>
                                                <asp:Label runat="server" ID="lbl_allocateuser" class="col-md-3"></asp:Label>                                               
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-3">Group</label>
                                                 <asp:Label runat="server" ID="lbl_group" class="col-md-3"></asp:Label>                                                
                                            </div>
                                            <div class="form-group m-b-0">
                                                <label class="col-md-3">Sub Group</label>
                                                 <asp:Label runat="server" ID="lbl_subgroup" class="col-md-3"></asp:Label>                                                
                                            </div>
                                      
                                           
                                    </div> <!-- end col -->

                                    <div class="col-md-6">
                                      
                                            <div class="form-group">
                                                <label class="col-md-3">End Date</label>
                                                 <asp:Label runat="server" ID="lbl_enddate" class="col-md-3"></asp:Label>                                                
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-3">Priority</label>
                                                 <asp:Label runat="server" ID="lbl_priorty" class="col-md-3"></asp:Label>                                                
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-3">Status</label>
                                                    <asp:Label runat="server" ID="lbl_status" class="col-md-3"></asp:Label>                                   
                                            </div>
                                           <div class="form-group m-b-0">
                                                <label class="col-md-3">Start Date</label>
                                              <asp:Label runat="server" ID="lbl_startdate" class="col-md-3"></asp:Label>
                                            </div>
                                          
                                    
                                       
                                    </div> <!-- end col -->
                                </div>
                                <!-- end row -->

                            </div> <!-- end card-body-->
                        </div> <!-- end card-->
                    </div> <!-- end col -->
                </div>
                        <div class="row" id="Task" runat="server" >
                    <div class="col-12">
                         <div class="card">
                            <div class="card-body" style="overflow: auto;">
                
                                <h4 class="card-title">Create Sub Task</h4>  
                       <table id="basic-datatable" class="table dt-responsive nowrap">
                            <thead>
                                <tr>
                                            <th> Sub Task Name
                                            </th>
                                            <th>Department
                                            </th>
                                            <th>User
                                            </th>
                                            <th>Group
                                            </th>
                                            <th>Sub Group                                                
                                            </th>  
                                            <th>Start Date                                                
                                            </th>  
                                            <th>End Date                                              
                                            </th> 
                                            <th>Priority                                              
                                            </th>                                            
                                            <th>Status                                             
                                            </th> 
                                            <th>Action
                                            </th>
                                        <%--    <th>Create Sub Task
                                            </th>
                                            <th>Create Milestone
                                            </th>--%>
                                        </tr>
                                </thead>
                            <tbody>
                                <tr style="background-color: White">
                                            <td>
                                                <div>
                                                    <div class="col-md-12">                                                       
                                                        <asp:TextBox runat="server" ID="txt_subtask" CssClass="form-control"></asp:TextBox>
                                                    </div>                                                  
                                                </div>
                                            </td>
                                              
                     
                                                <td>
                                                     <div class="form-group">
                                                          <div class="input-group col-md-12">
                                                                  <asp:DropDownList runat="server" ID="ddl_dept" CssClass="form-control"></asp:DropDownList>
                                                          </div>
                                                    </div>
                                                </td>
                 
                                            <td>
                                                <div class="form-group ">
                                                    <div class="input-group col-md-12">
                                                          <asp:DropDownList runat="server" ID="ddl_user" CssClass="form-control"></asp:DropDownList>
                                                    </div>                                                  
                                                </div>
                                            </td>
                                            <td>
                                                <div class="form-group ">
                                                    <div class="input-group col-md-12">
                                                      <asp:DropDownList runat="server" ID="ddl_group" CssClass="form-control"></asp:DropDownList>
                                                    </div>
                                                    <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator12" runat="server" ErrorMessage="*" ForeColor="Red"  ValidationGroup="addvalid"
                                                        ControlToValidate="ddl_rat3" SetFocusOnError="true" Display="Dynamic" InitialValue="0"></asp:RequiredFieldValidator>--%>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="form-group ">
                                                    <div class="input-group col-md-12">
                                                           <asp:DropDownList runat="server" ID="ddl_subgroup" CssClass="form-control"></asp:DropDownList>
                                                    </div>

                                                </div>
                                            </td>  
                                      <td>
                                                <div class="form-group ">
                                                    <div class="input-group col-md-12">
                                                      <asp:TextBox runat="server" ID="txt_startdate" CssClass="form-control" type="date"></asp:TextBox>
                                                    </div>
                                                 
                                                </div>
                                            </td>
                                            <td>
                                                <div class="form-group ">
                                                    <div class="input-group col-md-12">
                                                          <asp:TextBox runat="server" ID="txt_enddate" CssClass="form-control" type="date"></asp:TextBox>
                                                    </div>

                                                </div>
                                            </td>    
                                      <td>
                                                <div class="form-group ">
                                                    <div class="input-group col-md-12">
                                                      <asp:DropDownList runat="server" ID="ddl_taskpriority" CssClass="form-control"></asp:DropDownList>
                                                    </div>                                                
                                                </div>
                                            </td>
                                            <td>
                                                <div class="form-group ">
                                                    <div class="input-group col-md-12">
                                                           <asp:DropDownList runat="server" ID="ddl_taskstatus" CssClass="form-control"></asp:DropDownList>
                                                    </div>

                                                </div>
                                            </td>    
                                            <td>
                                                <div class="form-group has-success">
                                                    <button class="btn grp" causesvalidation="false" style="background-color: #FC1B00;border-color: #FC1B00;color: #fff;font-family:Arial;" type="button" runat="server" id="btn_contactadd" onserverclick="btn_contactadd_ServerClick" >
                                                        &nbsp;Add
                                                    </button>
                                                </div>
                                            </td>
                                  <%--   <td>
                                                <div class="form-group has-success">
                                                    <button class="btn grp" style="background-color: #FC1B00;border-color: #FC1B00;color: #fff;font-family:Arial;" type="button" runat="server" id="Button1" onserverclick="btn_contact1_ServerClick" >
                                                        &nbsp;Add sub Task
                                                    </button>
                                                </div>
                                            </td>
                                     <td>
                                                <div class="form-group has-success">
                                                    <button class="btn grp" style="background-color: #FC1B00;border-color: #FC1B00;color: #fff;font-family:Arial;" type="button" runat="server" id="Button2" onserverclick="btn_contact2_ServerClick" >
                                                        &nbsp;Add Milestone
                                                    </button>
                                                </div>
                                            </td>--%>
                                        </tr>
                            </tbody>
                                 <asp:Panel ID="pnl_productdetails" runat="server">
                                    <table class="table dt-responsive nowrap">
                                        <asp:Repeater ID="lv_items" runat="server" >
                                            <HeaderTemplate>
                                        <tr>
                                          <th>Task Name
                                            </th>
                                            <th>Department
                                            </th>
                                            <th>User
                                            </th>
                                            <th>Group
                                            </th>
                                            <th>Sub Group                                                
                                            </th>  
                                            <th>Start Date                                                
                                            </th>  
                                            <th>End Date                                              
                                            </th> 
                                            <th>Priority                                              
                                            </th>                                            
                                            <th>Status                                             
                                            </th> 
                                          
                                            <%--<th>Create Sub Task
                                            </th>
                                            <th>Create Milestone
                                            </th>--%>
                                        </tr>
                                                </HeaderTemplate>
                                            <ItemTemplate>
                                        <tr>
                                            <td>
                                                <div>
                                                    <div>
                                                          <asp:Label ID="lbl_taskname" runat="server" Text='<%# Eval("TaskName")%>'></asp:Label>
                                                    </div>
                                                </div>
                                            </td>
                                              
                     
                                                <td>
                                                     <div>
                                                          <div>
                                                                 <asp:Label ID="lbl_dept" runat="server" Text='<%# Eval("DepartmentId")%>'></asp:Label>
                                                          </div>
                                                    </div>
                                                </td>
                 
                                            <td>
                                                <div>
                                                    <div>
                                                         <asp:Label ID="lbl_user" runat="server" Text='<%# Eval("UserId")%>'></asp:Label>
                                                        
                                                    </div>
                                                   
                                                </div>
                                            </td>
                                            <td>
                                                <div>
                                                    <div>
                                                         <asp:Label ID="lbl_group" runat="server" Text='<%# Eval("GroupId")%>'></asp:Label>
                                                    </div>
                                                    
                                                </div>
                                            </td>
                                            <td>
                                                <div>
                                                    <div>
                                                       <asp:Label ID="lbl_subgroup" runat="server" Text='<%# Eval("SubGroupId")%>'></asp:Label>
                                                    </div>

                                                </div>
                                            </td>    
                                             <td>
                                                <div>
                                                    <div>
                                                         <asp:Label ID="lbl_startdate" runat="server" Text='<%# Eval("StartDate")%>'></asp:Label>
                                                    </div>
                                                    
                                                </div>
                                            </td>
                                            <td>
                                                <div>
                                                    <div>
                                                       <asp:Label ID="lbl_enddate" runat="server" Text='<%# Eval("EndDate")%>'></asp:Label>
                                                    </div>

                                                </div>
                                            </td>    
                                             <td>
                                                <div>
                                                    <div>
                                                       <asp:Label ID="lbl_priority" runat="server" Text='<%# Eval("Priority")%>'></asp:Label>
                                                    </div>

                                                </div>
                                            </td>  
                                              <td>
                                                <div>
                                                    <div>
                                                       <asp:Label ID="lbl_status" runat="server" Text='<%# Eval("Status")%>'></asp:Label>
                                                    </div>

                                                </div>
                                            </td>  
                                        </tr>
                                        </ItemTemplate>
                                            <FooterTemplate>
                                            </FooterTemplate>
                                        </asp:Repeater>
                                    </table>
                                </asp:Panel>


                                
                      
                                
                            
                            
                        </table>
                                   <div class="form-group m-b-0">
                                               
                                             <asp:Button runat="server" ID="btn_tasksubmit" OnClick="btn_tasksubmit_Click" class="btn btn-success waves-effect waves-light" Text="Submit" />
                                            </div>
                               
                                </div>
                             </div>
                        </div>
                            </div>


                    </div>
                </div>
                </div>
    </asp:Content>
