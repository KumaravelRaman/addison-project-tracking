﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;
using System.Data.SqlClient;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Collections.Generic;

namespace AddisonProjectTracking.Users
{
    public partial class CreateProject : System.Web.UI.Page
    {
        string id = "0";
        DataControl cls = new DataControl();
        SqlConnection Conn = new SqlConnection(ConfigurationManager.ConnectionStrings["conn"].ToString());
        protected void Page_Load(object sender, EventArgs e)
        {
            id = Request.QueryString["id"];
            if (!Page.IsPostBack)
            {
                Department();
                Priority();
                Purpose();
                ProjectViewAccess();
                ProjectStatus();
                Reqforchange();
                TaskDept();
                
                TaskPriority();
                TaskStatus();
                TaskGroup();
               
                if (id != "" && id != "0"&& id !=null)
                {
                    btn_update.Visible = true;
                    btn_submit.Visible = false;
                    id = cls.Decrypt(Request.QueryString["id"].ToString());
                    bind(id);
                }
                else
                {
                    btn_update.Visible = false;
                    btn_submit.Visible = true;
                }
              

            }
         }
        public void bind(string id)
        {                     
            string query = @"select * from tbl_project a where Id='" + id + "'";
            DataTable dt = cls.Getdata(query);
            if (dt.Rows.Count > 0)
            {
              
                Priority();
                txt_projectname.Text = dt.Rows[0]["ProjectName"].ToString();
                ddl_department.SelectedValue = dt.Rows[0]["DepartmentId"].ToString();
                ProjectOwner();
                ddl_projectowner.SelectedValue= dt.Rows[0]["ProjectOwner"].ToString();
                
                ddl_purposre.SelectedValue= dt.Rows[0]["PurposeOfProject"].ToString();
                ddl_priority.SelectedItem.Text = dt.Rows[0]["Priority"].ToString();
                txt_valueofproject.Text = dt.Rows[0]["ValueOfProject"].ToString();
                txt_targetDate.Text =Convert.ToDateTime(dt.Rows[0]["TargetDate"]).ToString("DD/MM/YYYY");
                txt_updateinterval.Text = dt.Rows[0]["UpdateInterval"].ToString();
                ddl_projviewaccess.SelectedValue= dt.Rows[0]["ProjectViewAccess"].ToString();
                ddl_status.SelectedValue = dt.Rows[0]["Status"].ToString();
                if (dt.Rows[0]["Status"].ToString() == "2" || dt.Rows[0]["Status"].ToString() == "4")
                {
                    Task.Visible = true;
                    ddl_status.SelectedValue = "5";
                    btn_update.Visible = false;
                }
                else
                    Task.Visible = false;

            }
            txt_projectname.Enabled = false;
            ddl_department.Enabled = false;            
            ddl_projectowner.Enabled = false;
            ddl_purposre.Enabled = false;
            ddl_priority.Enabled = false;
            txt_valueofproject.Enabled = false;
            txt_targetDate.Enabled = false;
            txt_updateinterval.Enabled = false;
            ddl_projviewaccess.Enabled = false;
            //ddl_status.SelectedValue = dt.Rows[0]["Status"].ToString();
        }
        //Create Project
        public void Reqforchange()
        {

            Conn.Open();
            string qry = "select Id,RequestforChange from tbl_requestforchange where Status='Active'";
            SqlDataAdapter dacname = new SqlDataAdapter(qry, Conn);
            DataSet dsname = new DataSet();
            dacname.Fill(dsname);
            if (dsname.Tables[0].Rows.Count > 0)
            {
                ddl_requestforchange.DataSource = dsname;
                ddl_requestforchange.DataTextField = "RequestforChange";
                ddl_requestforchange.DataValueField = "Id";
                ddl_requestforchange.DataBind();
                ddl_requestforchange.Items.Insert(0, "Select");

            }
            Conn.Close();
        }
        public void Priority()
        {
            ddl_priority.Items.Insert(0, "Select");
            ddl_priority.Items.Insert(1, "Normal");
            ddl_priority.Items.Insert(2, "Urgent");
        }       
        public void Department()
        {

            Conn.Open();
            string qry = "select Id,Department from tbl_department where Status='Active'";
            SqlDataAdapter dacname = new SqlDataAdapter(qry, Conn);
            DataSet dsname = new DataSet();
            dacname.Fill(dsname);
            if (dsname.Tables[0].Rows.Count > 0)
            {
                ddl_department.DataSource = dsname;
                ddl_department.DataTextField = "Department";
                ddl_department.DataValueField = "Id";
                ddl_department.DataBind();
                ddl_department.Items.Insert(0, "Select");

            }
            Conn.Close();
        }
        public void ProjectOwner()
        {

            Conn.Open();
            string qry = "select Id,EmployeeName from tbl_employee where Status='Active' and TypeId=2 and DepartmentId='"+ddl_department.SelectedValue+"'";
            SqlDataAdapter dacname = new SqlDataAdapter(qry, Conn);
            DataSet dsname = new DataSet();
            dacname.Fill(dsname);
            if (dsname.Tables[0].Rows.Count > 0)
            {
                ddl_projectowner.DataSource = dsname;
                ddl_projectowner.DataTextField = "EmployeeName";
                ddl_projectowner.DataValueField = "Id";
                ddl_projectowner.DataBind();
                ddl_projectowner.Items.Insert(0, "Select");

            }
            Conn.Close();
        }
        public void ProjectViewAccess()
        {

            Conn.Open();
            string qry = "select Id,EmployeeName from tbl_employee where Status='Active'";
            SqlDataAdapter dacname = new SqlDataAdapter(qry, Conn);
            DataSet dsname = new DataSet();
            dacname.Fill(dsname);
            if (dsname.Tables[0].Rows.Count > 0)
            {
                ddl_projviewaccess.DataSource = dsname;
                ddl_projviewaccess.DataTextField = "EmployeeName";
                ddl_projviewaccess.DataValueField = "Id";
                ddl_projviewaccess.DataBind();
                ddl_projviewaccess.Items.Insert(0, "Select");

            }
            Conn.Close();
        }
        public void Purpose()
        {

            Conn.Open();
            string qry = "select Id,PurposeofProject from tbl_purposeofproject where Status='Active'";
            SqlDataAdapter dacname = new SqlDataAdapter(qry, Conn);
            DataSet dsname = new DataSet();
            dacname.Fill(dsname);
            if (dsname.Tables[0].Rows.Count > 0)
            {
                ddl_purposre.DataSource = dsname;
                ddl_purposre.DataTextField = "PurposeofProject";
                ddl_purposre.DataValueField = "Id";
                ddl_purposre.DataBind();
                ddl_purposre.Items.Insert(0, "Select");

            }
            Conn.Close();
        }
        public void ProjectStatus()
        {

            Conn.Open();
            string qry = "select Id,ProjectStatus from tbl_projectstatus where Status='Active'";
            SqlDataAdapter dacname = new SqlDataAdapter(qry, Conn);
            DataSet dsname = new DataSet();
            dacname.Fill(dsname);
            if (dsname.Tables[0].Rows.Count > 0)
            {
                ddl_status.DataSource = dsname;
                ddl_status.DataTextField = "ProjectStatus";
                ddl_status.DataValueField = "Id";
                ddl_status.DataBind();
                ddl_status.Items.Insert(0, "Select");

            }
            Conn.Close();
        }
        protected void ddl_department_SelectedIndexChanged(object sender, EventArgs e)
        {
            ProjectOwner();
        }
        protected void btn_submit_Click(object sender, EventArgs e)
        {
            string ProjectCode = "";
            string queryy = @"select count(Id)cnt from tbl_project";
            DataTable dtt = cls.Getdata(queryy);
            if (dtt.Rows.Count > 0)
            {
                string prefix = "PJ";
                DateTime Date = DateTime.Now.Date;
                string Datee = Date.ToString("yyyy");                

                int suffix = Convert.ToInt16(dtt.Rows[0]["cnt"]);
                suffix = suffix + 1;
                string conn = "000";
                ProjectCode = prefix + '/' + Datee + '/' + conn + suffix;
                Session["tktno"] = ProjectCode;
                Session["Date"] = Datee;
            }
            string qry = @"Insert into tbl_project (ProjectCode,ProjectName,DepartmentId,ProjectOwner,RaisedBy,PurposeOfProject,Priority,ValueOfProject,TargetDate,UpdateInterval,ProjectViewAccess,UploadFile,Status,CreatedOn,CreatedBy,UpdatedOn,UpdatedBy)values
            (@ProjectCode,@ProjectName,@DepartmentId,@ProjectOwner,@RaisedBy,@PurposeOfProject,@Priority,@ValueOfProject,@TargetDate,@UpdateInterval,@ProjectViewAccess,@UploadFile,@Status,@CreatedOn,@CreatedBy,@UpdatedOn,@UpdatedBy)";
            SqlCommand cmd = new SqlCommand(qry, Conn);
            Conn.Open();
           
            cmd.Parameters.AddWithValue("@ProjectCode", ProjectCode);
            cmd.Parameters.AddWithValue("@ProjectName", txt_projectname.Text);
            cmd.Parameters.AddWithValue("@DepartmentId", ddl_department.SelectedValue);
            cmd.Parameters.AddWithValue("@ProjectOwner", ddl_projectowner.SelectedValue);
            cmd.Parameters.AddWithValue("@RaisedBy", Session["UserName"]);
            

            cmd.Parameters.AddWithValue("@PurposeOfProject", ddl_purposre.SelectedValue);
            cmd.Parameters.AddWithValue("@Priority", ddl_priority.SelectedItem.Text);
            cmd.Parameters.AddWithValue("@ValueOfProject", txt_valueofproject.Text);
            cmd.Parameters.AddWithValue("@TargetDate", txt_targetDate.Text);

            cmd.Parameters.AddWithValue("@UpdateInterval", txt_updateinterval.Text);
            cmd.Parameters.AddWithValue("@ProjectViewAccess", ddl_projviewaccess.SelectedValue);
            cmd.Parameters.AddWithValue("@UploadFile", "");
            cmd.Parameters.AddWithValue("@Status", ddl_status.SelectedValue);

            cmd.Parameters.AddWithValue("@CreatedOn", DateTime.Now);
            cmd.Parameters.AddWithValue("@CreatedBy", "");
            cmd.Parameters.AddWithValue("@UpdatedOn", DateTime.Now);
            cmd.Parameters.AddWithValue("@UpdatedBy", "");

            cmd.ExecuteNonQuery();
            Conn.Close();
            Response.Redirect("ProjectCode.aspx");
            //ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "usernotfound", "alert('" + "Saved Successfully" + "'); document.location.href='SearchCompany.aspx';", true);
        }

        //Create task
        public void TaskDept()
        {

            Conn.Open();
            string qry = "select Id,Department from tbl_department where Status='Active'";
            SqlDataAdapter dacname = new SqlDataAdapter(qry, Conn);
            DataSet dsname = new DataSet();
            dacname.Fill(dsname);
            if (dsname.Tables[0].Rows.Count > 0)
            {
                ddl_dept.DataSource = dsname;
                ddl_dept.DataTextField = "Department";
                ddl_dept.DataValueField = "Id";
                ddl_dept.DataBind();
                ddl_dept.Items.Insert(0, "Select");

            }
            Conn.Close();
        }
        public void TaskUser()
        {

            Conn.Open();
            string qry = "select Id,EmployeeName from tbl_employee where Status='Active' and TypeId !=1 and DepartmentId='"+ddl_dept.SelectedValue+"'";
            SqlDataAdapter dacname = new SqlDataAdapter(qry, Conn);
            DataSet dsname = new DataSet();
            dacname.Fill(dsname);
            if (dsname.Tables[0].Rows.Count > 0)
            {
                ddl_user.DataSource = dsname;
                ddl_user.DataTextField = "EmployeeName";
                ddl_user.DataValueField = "Id";
                ddl_user.DataBind();
                ddl_user.Items.Insert(0, "Select");

            }
            Conn.Close();
        }
        public void TaskPriority()
        {

            ddl_taskpriority.Items.Insert(0, "Select");
            ddl_taskpriority.Items.Insert(1, "Low");
            ddl_taskpriority.Items.Insert(2, "Medium");
            ddl_taskpriority.Items.Insert(2, "High");
        }
        public void TaskStatus()
        {

            ddl_taskstatus.Items.Insert(0, "Select");
            ddl_taskstatus.Items.Insert(1, "Allocated");
            ddl_taskstatus.Items.Insert(2, "Pending");
            ddl_taskstatus.Items.Insert(2, "Completed");
        }
        public void TaskGroup()
        {

            Conn.Open();
            string qry = "select Id,Division from tbl_division where Status='Active'";
            SqlDataAdapter dacname = new SqlDataAdapter(qry, Conn);
            DataSet dsname = new DataSet();
            dacname.Fill(dsname);
            if (dsname.Tables[0].Rows.Count > 0)
            {
                ddl_group.DataSource = dsname;
                ddl_group.DataTextField = "Division";
                ddl_group.DataValueField = "Id";
                ddl_group.DataBind();
                ddl_group.Items.Insert(0, "Select");

            }
            Conn.Close();
        }
        public void TaskSubGroup()
        {

            Conn.Open();
            string qry = "select Id,SubDivision from tbl_subdivision where Status='Active' and DivisionId='"+ddl_group.SelectedValue+"'";
            SqlDataAdapter dacname = new SqlDataAdapter(qry, Conn);
            DataSet dsname = new DataSet();
            dacname.Fill(dsname);
            if (dsname.Tables[0].Rows.Count > 0)
            {
                ddl_subgroup.DataSource = dsname;
                ddl_subgroup.DataTextField = "SubDivision";
                ddl_subgroup.DataValueField = "Id";
                ddl_subgroup.DataBind();
                ddl_subgroup.Items.Insert(0, "Select");

            }
            Conn.Close();
        }
        protected void btn_contactadd_ServerClick(object sender, EventArgs e)
        {
            ddl_status.SelectedValue = "5";
            Cls_taskdetails lclsIsExist = lCls_Productitems.Find(delegate (Cls_taskdetails filedata)
            {
                return filedata.TaskName == (Convert.ToString(txt_subtask.Text));

            });
            if (lclsIsExist == null)
            {
                if (ddl_user.SelectedItem.Text != "Select" && ddl_taskstatus.SelectedItem.Text != "Select" && ddl_taskpriority.SelectedItem.Text != "Select" && ddl_group.SelectedItem.Text != "Select" && ddl_subgroup.SelectedItem.Text != "Select")
                {
                    lclsIsExist = new Cls_taskdetails();
                    lclsIsExist.TaskName = Convert.ToString(txt_subtask.Text);
                    lclsIsExist.DepartmentId = Convert.ToString(ddl_dept.SelectedItem.Text);
                    lclsIsExist.UserId = Convert.ToString(ddl_user.SelectedItem.Text);
                    lclsIsExist.GroupId = Convert.ToString(ddl_group.SelectedItem.Text);
                    lclsIsExist.SubGroupId = Convert.ToString(ddl_subgroup.SelectedItem.Text);                    
                    lclsIsExist.StartDate = Convert.ToDateTime(txt_startdate.Text).Date;
                    lclsIsExist.EndDate = Convert.ToDateTime(txt_enddate.Text).Date;
                    lclsIsExist.Priority = Convert.ToString(ddl_taskpriority.SelectedItem.Text);
                    lclsIsExist.Status = Convert.ToString(ddl_taskstatus.SelectedItem.Text);

                    lCls_Productitems.Add(lclsIsExist);
                    lv_items.DataSource = lCls_Productitems;
                    lv_items.DataBind();

                    //ddl_prod.ClearSelection();
                    ddl_dept.ClearSelection();
                    ddl_user.Items.Clear();
                    ddl_subgroup.ClearSelection();
                    ddl_group.ClearSelection();
                    txt_subtask.Text = "";
                    txt_startdate.Text = "";
                    txt_enddate.Text = "";
                    ddl_taskpriority.ClearSelection();
                    ddl_taskstatus.ClearSelection();
                    
                }

                else
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "alertMessage", "alert('Please select all the column');", true);
                }
            }

        }
        public List<Cls_taskdetails> lCls_Productitems
        {
            get
            {
                if (this.ViewState["productItems"] == null)
                    this.ViewState["productItems"] = new List<Cls_taskdetails>();

                return (
                    List<Cls_taskdetails>)this.ViewState["productItems"];
            }
            set
            {
                this.ViewState["productItem"] = value;
            }
        }
        [Serializable]
        public class Cls_taskdetails
        {
            public string TaskName { get; set; }
            public string DepartmentId { get; set; }
            public string UserId { get; set; }
            public string GroupId { get; set; }
            public string SubGroupId { get; set; }
            public DateTime StartDate { get; set; }
            public DateTime EndDate { get; set; }
            public string Priority { get; set; }
            public string Status { get; set; }
        }

        protected void btn_tasksubmit_Click(object sender, EventArgs e)
        {
            id = cls.Decrypt(Request.QueryString["id"].ToString());
            string qryy = @"Update tbl_project set Status =@Status,UpdatedOn=@UpdatedOn,UpdatedBy=@UpdatedBy where Id = @Id";
            SqlCommand cmd = new SqlCommand(qryy, Conn);
            //Conn.Open();

            cmd.Parameters.AddWithValue("@Status", ddl_status.SelectedValue);
            cmd.Parameters.AddWithValue("@CreatedOn", DateTime.Now);
            cmd.Parameters.AddWithValue("@Id", id);
            cmd.Parameters.AddWithValue("@UpdatedOn", DateTime.Now);
            cmd.Parameters.AddWithValue("@UpdatedBy", Session["UserName"]);

            Conn.Open();
            cmd.ExecuteNonQuery();
            Conn.Close();
            //id = cls.Decrypt(Request.QueryString["id"].ToString());
            foreach (RepeaterItem item in lv_items.Items)
            {
                Label TaskName = (Label)item.FindControl("lbl_taskname");
                Label Department = (Label)item.FindControl("lbl_dept");
                Label User = (Label)item.FindControl("lbl_user");
                Label Group = (Label)item.FindControl("lbl_group");
                Label SubGroup = (Label)item.FindControl("lbl_subgroup");
                Label startdate = (Label)item.FindControl("lbl_startdate");
                Label enddate = (Label)item.FindControl("lbl_enddate");
                Label priority = (Label)item.FindControl("lbl_priority");
                Label status = (Label)item.FindControl("lbl_status");

                string qry = @"Insert into tbl_subproject (ProjectId,TaskName,Department,AllocateUser,TaskGroup,SubGroup,StartDate,EndDate,Priority,IsLeveltwo,IsMilestone,UploadFile,Status,CreatedOn,CreatedBy,UpdatedOn,UpdatedBy)values
            (@ProjectId,@TaskName,@Department,@AllocateUser,@TaskGroup,@SubGroup,@StartDate,@EndDate,@Priority,@IsLeveltwo,@IsMilestone,@UploadFile,@Status,@CreatedOn,@CreatedBy,@UpdatedOn,@UpdatedBy)";
                SqlCommand cmdcon = new SqlCommand(qry, Conn);
                Conn.Open();
                cmdcon.Parameters.AddWithValue("@ProjectId", id);
                cmdcon.Parameters.AddWithValue("@TaskName", TaskName.Text);
                cmdcon.Parameters.AddWithValue("@Department", Department.Text);
                cmdcon.Parameters.AddWithValue("@AllocateUser", User.Text);
                cmdcon.Parameters.AddWithValue("@TaskGroup", Group.Text);
                cmdcon.Parameters.AddWithValue("@SubGroup", SubGroup.Text);
                cmdcon.Parameters.AddWithValue("@StartDate", Convert.ToDateTime( startdate.Text));
                cmdcon.Parameters.AddWithValue("@EndDate", Convert.ToDateTime(enddate.Text));
                cmdcon.Parameters.AddWithValue("@Priority", priority.Text);
                cmdcon.Parameters.AddWithValue("@IsLeveltwo", false);
                cmdcon.Parameters.AddWithValue("@IsMilestone", false);
                cmdcon.Parameters.AddWithValue("@UploadFile", "");
                cmdcon.Parameters.AddWithValue("@Status", status.Text);
                cmdcon.Parameters.AddWithValue("@CreatedOn", DateTime.Now);
                cmdcon.Parameters.AddWithValue("@CreatedBy", Session["UserName"]);
                cmdcon.Parameters.AddWithValue("@UpdatedOn", DateTime.Now);
                cmdcon.Parameters.AddWithValue("@UpdatedBy", "");

                //Conn.Open();
                cmdcon.ExecuteNonQuery();
                Conn.Close();
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "usernotfound", "alert('" + "Sub Project Created Successfully" + "'); document.location.href='SearchProject.aspx';", true);

            }
        }

        protected void btn_update_Click(object sender, EventArgs e)
        {
            id = cls.Decrypt(Request.QueryString["id"].ToString());
            string qry = @"Update tbl_project set Status =@Status,UpdatedOn=@UpdatedOn,UpdatedBy=@UpdatedBy where Id = @Id";
            SqlCommand cmdcon = new SqlCommand(qry, Conn);
            //Conn.Open();

            cmdcon.Parameters.AddWithValue("@Status", ddl_status.SelectedValue);
            cmdcon.Parameters.AddWithValue("@CreatedOn", DateTime.Now);
            cmdcon.Parameters.AddWithValue("@Id",id );
            cmdcon.Parameters.AddWithValue("@UpdatedOn", DateTime.Now);
            cmdcon.Parameters.AddWithValue("@UpdatedBy", Session["UserName"]);

            Conn.Open();
            cmdcon.ExecuteNonQuery();
            Conn.Close();
            if(ddl_status.SelectedValue=="3")
            {
                string query = @"Insert into tbl_project_transit(ProjectId,RequestDate,ReasonId,UpdatedOn,UpdatedBy)values(@ProjectId,@RequestDate,@ReasonId,@UpdatedOn,@UpdatedBy)";
                SqlCommand cmd = new SqlCommand(query, Conn);
                //Conn.Open();

                cmd.Parameters.AddWithValue("@ProjectId", id);
                cmd.Parameters.AddWithValue("@RequestDate", txt_requesteddate.Text);
                cmd.Parameters.AddWithValue("@ReasonId", ddl_requestforchange.SelectedValue);
                cmd.Parameters.AddWithValue("@UpdatedOn", DateTime.Now);
                cmd.Parameters.AddWithValue("@UpdatedBy", Session["UserName"]);

                Conn.Open();
                cmd.ExecuteNonQuery();
                Conn.Close();
            }
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "usernotfound", "alert('" + "Updated Successfully" + "'); document.location.href='SearchProject.aspx';", true);
        }

        protected void ddl_status_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddl_status.SelectedItem.Text == "Rejected")
            {
                reject.Visible = true;
                reason.Visible = true;
            }
            else
            {
                reject.Visible = false;
                reason.Visible = false;
            }
        }

        protected void ddl_dept_SelectedIndexChanged(object sender, EventArgs e)
        {
            TaskUser();
        }

        protected void ddl_group_SelectedIndexChanged(object sender, EventArgs e)
        {
            TaskSubGroup();
        }
    }
}