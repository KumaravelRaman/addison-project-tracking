﻿<%@ Page Title="Dashboard" Language="C#" AutoEventWireup="true" CodeBehind="Dashboard.aspx.cs" MasterPageFile="~/Users/UserLayout.Master" Inherits="AddisonProjectTracking.Users.Dashboard" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

  
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">   
    <div class="main-content">

            <div class="page-content">
                <div class="container-fluid">

                    <!-- start page title -->
                    <div class="row">
                        <div class="col-12">
                            <div class="page-title-box d-flex align-items-center justify-content-between">
                                <h4 class="mb-0 font-size-18">Dashboard</h4>

                                <div class="page-title-right">
                                    <ol class="breadcrumb m-0">
                                        <li class="breadcrumb-item"><a href="javascript: void(0);">Project</a></li>
                                        <li class="breadcrumb-item active">Dashboard</li>
                                    </ol>
                                </div>

                            </div>
                        </div>
                    </div>
                    <!-- end page title -->
                    <div class="row">
                        <div class="col-lg-6 col-xl-3">
                            <div class="card">
                                <div class="card-body">
                                    <div class="row align-items-center">
                                        <div class="col">
                                            <h6 class="text-uppercase font-size-12 text-muted mb-3">New Project</h6>
                                            <span class="h3 mb-0"> 10</span>
                                        </div>
                                        <div class="col-auto">
                                            <%--<span class="badge badge-soft-success">+7.5%</span>--%>
                                        </div>
                                    </div> <!-- end row -->
    
                                    <div id="sparkline1" class="mt-3"></div>
                                </div> <!-- end card-body-->
                            </div> <!-- end card-->
                        </div> <!-- end col-->
                        
                        <div class="col-lg-6 col-xl-3">
                            <div class="card">
                                <div class="card-body">
                                    <div class="row align-items-center">
                                        <div class="col">
                                            <h6 class="text-uppercase font-size-12 text-muted mb-3">Accepted</h6>
                                            <span class="h3 mb-0">25 </span>
                                        </div>
                                        <div class="col-auto">
                                            <%--<span class="badge badge-soft-danger">12</span>--%>
                                        </div>
                                    </div> <!-- end row -->
    
                                    <div id="sparkline2" class="mt-3"></div>
                                </div> <!-- end card-body-->
                            </div> <!-- end card-->
                        </div> <!-- end col-->
    
                        <div class="col-lg-6 col-xl-3">
                            <div class="card">
                                <div class="card-body">
                                    <div class="row align-items-center">
                                        <div class="col">
                                            <h6 class="text-uppercase font-size-12 text-muted mb-3">Rejected</h6>
                                            <span class="h3 mb-0"> 2</span>
                                        </div>
                                        <div class="col-auto">
                                            <%--<span class="badge badge-soft-success">2</span>--%>
                                        </div>
                                    </div> <!-- end row -->
    
                                    <div id="sparkline3" class="mt-3"></div>
                                </div> <!-- end card-body-->
                            </div> <!-- end card-->
                        </div> <!-- end col-->
    
                        <div class="col-lg-6 col-xl-3">
                            <div class="card">
                                <div class="card-body">
                                    <div class="row align-items-center">
                                        <div class="col">
                                            <h6 class="text-uppercase font-size-12 text-muted mb-3">Completed Project</h6>
                                            <span class="h3 mb-0"> 22</span>
                                        </div>
                                        <div class="col-auto">
                                            <%--<span class="badge badge-soft-success">+53.5%</span>--%>
                                        </div>
                                    </div> <!-- end row -->
    
                                    <div id="sparkline4" class="mt-3"></div>
                                </div> <!-- end card-body-->
                            </div> <!-- end card-->
                        </div> <!-- end col-->
                    </div>
                    <!-- end row-->

                    <div class="row" style="display:none;">

                        <div class="col-lg-4" >
                             <div class="card card-animate">
                                <div class="card-body">
                                    <div class="dropdown float-right position-relative">
                                        <a href="#" class="dropdown-toggle h4 text-muted" data-toggle="dropdown"
                                            aria-expanded="false">
                                            <i class="mdi mdi-dots-vertical"></i>
                                        </a>
                                        <ul class="dropdown-menu dropdown-menu-right">
                                            <li><a href="#" class="dropdown-item">Action</a></li>
                                            <li><a href="#" class="dropdown-item">Another action</a></li>
                                            <li><a href="#" class="dropdown-item">Something else here</a></li>
                                            <li class="dropdown-divider"></li>
                                            <li><a href="#" class="dropdown-item">Separated link</a></li>
                                        </ul>
                                    </div>
                                    <h4 class="card-title d-inline-block">Total Revenue</h4>

                                    <div id="morris-line-example" class="morris-chart" style="height: 320px;"></div>

                                    <div class="row text-center mt-4">
                                        <div class="col-6">
                                            <h4>$7841.12</h4>
                                            <p class="text-muted mb-0">Total Revenue</p>
                                        </div>
                                        <div class="col-6">
                                            <h4>17</h4>
                                            <p class="text-muted mb-0">Open Compaign</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> <!-- end col -->

                        <div class="col-lg-4">
                             <div class="card card-animate">
                                <div class="card-body">
                                    <div class="dropdown float-right position-relative">
                                        <a href="#" class="dropdown-toggle h4 text-muted" data-toggle="dropdown"
                                            aria-expanded="false">
                                            <i class="mdi mdi-dots-vertical"></i>
                                        </a>
                                        <ul class="dropdown-menu dropdown-menu-right">
                                            <li><a href="#" class="dropdown-item">Action</a></li>
                                            <li><a href="#" class="dropdown-item">Another action</a></li>
                                            <li><a href="#" class="dropdown-item">Something else here</a></li>
                                            <li class="dropdown-divider"></li>
                                            <li><a href="#" class="dropdown-item">Separated link</a></li>
                                        </ul>
                                    </div>
                                    <h4 class="card-title d-inline-block">All Time Best Products</h4>

                                    <div id="morris-donut-example" class="morris-chart" style="height: 320px;"></div>

                                    <div class="row text-center mt-4">
                                        <div class="col-6">
                                            <h4>5,459</h4>
                                            <p class="text-muted mb-0">Total Sales</p>
                                        </div>
                                        <div class="col-6">
                                            <h4>18</h4>
                                            <p class="text-muted mb-0">Open Compaign</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> <!-- end col -->
                        <div class="col-xl-4">
                            <div class="card card-animate">
                               <div class="card-body">
                   
                                   <h4 class="card-title">USA Map</h4>
                                   <p class="card-subtitle mb-4">Example of united state of america map.</p>
                                   
                                   <div id="usa-map" style="height: 358px"></div>
    
                               </div> <!-- end card-body-->
                           </div> <!-- end card-->
                       </div> <!-- end col -->
                    </div>

                    
                    <!-- end row-->

                    <div class="row">
                        <div class="col-lg-4" style="display:none;">
                             <div class="card card-animate">
                                <div class="card-body">
                                    <div class="dropdown float-right position-relative">
                                        <a href="#" class="dropdown-toggle h4 text-muted" data-toggle="dropdown"
                                            aria-expanded="false">
                                            <i class="mdi mdi-dots-vertical"></i>
                                        </a>
                                        <ul class="dropdown-menu dropdown-menu-right">
                                            <li><a href="#" class="dropdown-item">Action</a></li>
                                            <li><a href="#" class="dropdown-item">Another action</a></li>
                                            <li><a href="#" class="dropdown-item">Something else here</a></li>
                                            <li class="dropdown-divider"></li>
                                            <li><a href="#" class="dropdown-item">Separated link</a></li>
                                        </ul>
                                    </div>
                                    <h4 class="card-title d-inline-block mb-3">Team Members - Messages</h4>

                                    <div data-simplebar style="max-height: 410px;">
                                        <a href="#" class="d-flex align-items-center border-bottom py-3">
                                            <div class="mr-3">
                                                <img src="assets/images/users/avatar-2.jpg" class="rounded-circle avatar-sm" alt="user">
                                            </div>
                                            <div class="w-100">
                                                <div class="d-flex justify-content-between">
                                                    <h6 class="mb-1">Leonardo Payne</h6>
                                                    <p class="text-muted font-size-11 mb-0">12.30 PM</p>
                                                </div>
                                                <p class="text-muted font-size-13 mb-0">Hey! there I'm available...</p>
                                            </div>
                                        </a>
    
                                        <a href="#" class="d-flex align-items-center border-bottom py-3">
                                            <div class="mr-3">
                                                <img src="assets/images/users/avatar-3.jpg" class="rounded-circle avatar-sm" alt="user">
                                            </div>
                                            <div class="w-100">
                                                <div class="d-flex justify-content-between">
                                                    <h6 class="mb-1">Soren Drouin</h6>
                                                    <p class="text-muted font-size-11 mb-0">09.30 PM</p>
                                                </div>
                                                <p class="text-muted font-size-13 mb-0">Completed "Design new idea"....</p>
                                            </div>
                                        </a>
    
                                        <a href="#" class="d-flex align-items-center border-bottom py-3">
                                            <div class="mr-3">
                                                <img src="assets/images/users/avatar-4.jpg" class="rounded-circle avatar-sm" alt="user">
                                            </div>
                                            <div class="w-100">
                                                <div class="d-flex justify-content-between">
                                                    <h6 class="mb-1">Anne Simard</h6>
                                                    <p class="text-muted font-size-11 mb-0">10.30 PM</p>
                                                </div>
                                                <p class="text-muted font-size-13 mb-0">Assigned task "Poster illustation design"...</p>
                                            </div>
                                        </a>
    
                                        <a href="#" class="d-flex align-items-center border-bottom py-3">
                                            <div class="mr-3">
                                                <img src="assets/images/users/avatar-5.jpg" class="rounded-circle avatar-sm" alt="user">
                                            </div>
                                            <div class="w-100">
                                                <div class="d-flex justify-content-between">
                                                    <h6 class="mb-1">Nicolas Chartier</h6>
                                                    <p class="text-muted font-size-11 mb-0">02.00 PM</p>
                                                </div>
                                                <p class="text-muted font-size-13 mb-0">Completed "Drinking bottle graphics"...</p>
                                            </div>
                                        </a>
    
                                        <a href="#" class="d-flex align-items-center border-bottom py-3">
                                            <div class="mr-3">
                                                <img src="assets/images/users/avatar-6.jpg" class="rounded-circle avatar-sm" alt="user">
                                            </div>
                                            <div class="w-100">
                                                <div class="d-flex justify-content-between">
                                                    <h6 class="mb-1">Gano Cloutier</h6>
                                                    <p class="text-muted font-size-11 mb-0">05.30 PM</p>
                                                </div>
                                                <p class="text-muted font-size-13 mb-0">Assigned task "Hyper app design"...</p>
                                            </div>
                                        </a>

                                        <a href="#" class="d-flex align-items-center border-bottom py-3">
                                            <div class="mr-3">
                                                <img src="assets/images/users/avatar-2.jpg" class="rounded-circle avatar-sm" alt="user">
                                            </div>
                                            <div class="w-100">
                                                <div class="d-flex justify-content-between">
                                                    <h6 class="mb-1">Leonardo Payne</h6>
                                                    <p class="text-muted font-size-11 mb-0">12.30 PM</p>
                                                </div>
                                                <p class="text-muted font-size-13 mb-0">Hey! there I'm available...</p>
                                            </div>
                                        </a>
                                    </div>

                                </div>
                            </div>
                        </div> <!-- end col -->

                        <div class="col-lg-12">
                             <div class="card card-animate">
                                <div class="card-body">
                                    <div class="dropdown float-right position-relative">
                                        <a href="#" class="dropdown-toggle h4 text-muted" data-toggle="dropdown"
                                            aria-expanded="false">
                                            <i class="mdi mdi-dots-vertical"></i>
                                        </a>
                                        <ul class="dropdown-menu dropdown-menu-right">
                                            <li><a href="#" class="dropdown-item">Action</a></li>
                                            <li><a href="#" class="dropdown-item">Another action</a></li>
                                            <li><a href="#" class="dropdown-item">Something else here</a></li>
                                            <li class="dropdown-divider"></li>
                                            <li><a href="#" class="dropdown-item">Separated link</a></li>
                                        </ul>
                                    </div>
                                    <h4 class="card-title d-inline-block">All Projects</h4>

                                    <div class="table-responsive">
                                        <table class="table table-borderless table-hover mb-0">
                                            <thead class="thead-light">
                                                <tr>
                                                    <th>No</th>
                                                    <th>Project Name</th>
                                                    <th>Project Owner</th>
                                                    <th>Deadline</th>
                                                    <th>Percentage of Completion</th>
                                                    <th>Status</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <th>1</th>
                                                    <td>App design and development</td>
                                                    <td>Senthamizh</td>
                                                    <td>Sun, 08/10</td>
                                                    <td>50%</td>
                                                    <td class="text-warning">In Process</td>
                                                </tr>
                                                <tr>
                                                    <th>2</th>
                                                    <td>Drill Sharpening</td>
                                                    <td>Krishnan</td>
                                                    <td>Fri, 17/10</td>
                                                    <td>100%</td>
                                                    <td class="text-success">Done</td>
                                                </tr>
                                                <tr>
                                                    <th>3</th>
                                                    <td>Power Gear</td>
                                                    <td>James</td>
                                                    <td>Tue, 13/08</td>
                                                    <td>75%</td>
                                                    <td class="text-danger">Hold</td>
                                                </tr>
                                                <tr>
                                                    <th>4</th>
                                                    <td>Cutters Sharpening</td>
                                                    <td>Alice</td>
                                                    <td>Mon, 10/08</td>
                                                    <td>100%</td>
                                                    <td class="text-success">Done</td>
                                                </tr>
                                                <tr>
                                                    <th>5</th>
                                                    <td>Landing page design</td>
                                                    <td>Sam</td>
                                                    <td>Thus, 03/09</td>
                                                    <td>60%</td>
                                                    <td class="text-warning">In Process</td>
                                                </tr>
                                                <tr>
                                                    <th>6</th>
                                                    <td>Company logo design</td>
                                                    <td>Karthick</td>
                                                    <td>Sat, 07/19</td>
                                                    <td>100%</td>
                                                    <td class="text-success">Done</td>
                                                </tr>
                                                <tr>
                                                    <th>7</th>
                                                    <td>Product page redesign</td>
                                                    <td>Antony</td>
                                                    <td>Wed, 09/08</td>
                                                    <td>90%</td>
                                                    <td class="text-danger">Hold</td>
                                                </tr>
                                                <tr>
                                                    <th>8</th>
                                                    <td>New Machine launch</td>
                                                    <td>Salinas</td>
                                                    <td>Sun, 02/20</td>
                                                    <td>100%</td>
                                                    <td class="text-success">Done</td>
                                                </tr>
                                                
                                            </tbody>
                                        </table>
                                    </div>

                                </div>
                            </div>
                        </div> <!-- end col -->

                    </div>
                    <!-- end row-->

                   
                        
                   
                    </div>
                    
                </div> <!-- container-fluid -->
            </div>
    </asp:Content>
