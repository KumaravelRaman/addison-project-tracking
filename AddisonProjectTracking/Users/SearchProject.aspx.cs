﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.IO;

namespace AddisonProjectTracking.Users
{
    public partial class SearchProject : System.Web.UI.Page
    {
        DataControl cls = new DataControl();
        SqlConnection Conn = new SqlConnection(ConfigurationManager.ConnectionStrings["conn"].ToString());
        protected void Page_Load(object sender, EventArgs e)
        {
            bind_product();

        }
        public void bind_product()
        {
            lstv_productdata.DataSource = Search();
            lstv_productdata.DataBind();
        }
        protected DataTable Search()
        {
            string query = string.Empty;

            query = "select ROW_NUMBER() OVER (ORDER BY Id)[RowNumber],*,(select ProjectStatus from tbl_projectStatus b where b.id=a.Status)statuss,(select EmployeeName from tbl_employee c where c.id=a.ProjectOwner)ProjectOwnerr from tbl_project a where RaisedBy='" + Session["UserName"]+ "' or ProjectOwner = (select Id from tbl_Employee where EmployeeName ='"+ Session["UserName"] + "')  or ((select Distinct(AllocateUser) from tbl_subproject b where b.projectid=1 and AllocateUser='"+Session["UserName"]+ "') ='" + Session["UserName"] + "')";
        
            DataTable dt = cls.Getdata(query);
            return dt;
        }
        public string id;
        protected void lstv_productdata_ItemCommand(object sender, ListViewCommandEventArgs e)
        {
            if (e.CommandName == "edit")
            {
                id = e.CommandArgument.ToString();
                Response.Redirect("CreateProject.aspx?id=" + cls.Encrypt(id));
            }
            if (e.CommandName == "add")
            {
                id = e.CommandArgument.ToString();
                Response.Redirect("UpdateTask.aspx?id=" + cls.Encrypt(id));
            }
            if (e.CommandName == "view")
            {
                //id = e.CommandArgument.ToString();
                //Response.Redirect(".aspx?id=" + cls.Encrypt(id));
            }
        }
    }
}