﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using System.Web.UI;
using System.Net;
using System.Net.Mail;
using System.Web.UI.WebControls;

namespace AddisonProjectTracking
{
    public partial class Forgotpass : System.Web.UI.Page
    {
        string cs = ConfigurationManager.ConnectionStrings["conn"].ConnectionString;
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnForgot_Click(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection(cs);

            try
            {
                SqlCommand cmd = new SqlCommand("sp_b_adminlogin", con);

                cmd.CommandType = CommandType.StoredProcedure;
                con.Open();
                cmd.Parameters.AddWithValue("@qtype", "forgotlist");
                cmd.Parameters.AddWithValue("@email", txtForgotPassword.Text);
                
                SqlDataReader sdr = cmd.ExecuteReader();
                if (sdr.Read())
                {
                    Session["UserName"] = sdr["UserName"].ToString();
                    Session["Password"] = sdr["Password"].ToString();

                    string Uname = Session["UserName"].ToString();
                    string Password = Session["Password"].ToString();
                    string Email = txtForgotPassword.Text;

                     mail1(Password, Uname, Email);
                    Response.Redirect("~/AdminLogin.aspx");

                }
                else
                {
                    Label1.Text = "Sorry Invalid Email";
                }
            }
            catch (Exception ex)
            {
                Response.Write(ex);
            }
            finally
            {
                con.Close();

            }
           
           
        }
        public int mail1(string pswd, string user, string mailpass)
        {
            int i = 0;

            try
            {
                DateTime dte = DateTime.Today;
                var dt = dte.ToString("dd/MM/yyyy");
                MailMessage mail = new MailMessage();
                SmtpClient SmtpServer = new SmtpClient();
                mail.From = new MailAddress("mail@brainmagic.info");
                mail.From = new MailAddress("admin@brainmagic.info");
                mail.To.Add(mailpass);
                mail.Subject = "Login credentials"; // Mail Subject
                string body;
                body = "Dear " + user + ", <br/>";
                body += "<br/>Please find below Login details";
                body += "<br/>Your username - " + user + " , password -" + pswd;
                body += "<br/>Thanks & Regards,<br>Addison&Co.Ltd<br>http://www.addison.co.in/";
                mail.Body = body;
                //mail.AlternateViews.Add(Mail_Body());
                mail.IsBodyHtml = true;
                //System.Net.Mail.Attachment attachment;
                //attachment = new System.Net.Mail.Attachment(file); //Attaching File to Mail  
                //mail.Attachments.Add(attachment);
                SmtpServer.Host = "webmail.brainmagic.info"; //PORT  
                SmtpServer.Port = Convert.ToInt32(25); //PORT  

                SmtpServer.DeliveryMethod = SmtpDeliveryMethod.Network;
                SmtpServer.UseDefaultCredentials = false;
                // SmtpServer.Credentials = new NetworkCredential("mail@brainmagic.info", "kp0L2r3!");
                SmtpServer.Credentials = new NetworkCredential("admin@brainmagic.info", "2vdd40#W");
                SmtpServer.Send(mail);
            }
            catch (Exception ex)
            {
                i = 0;
            }
            return i;
        }

    }
}